package com.ghani.skripsi.model;

public class OptionModel {
    int idOption;
    int idQuestion;
    String option;
    int value;

    public OptionModel() {
    }

    public OptionModel(int idOption, int idQuestion, String option, int value) {
        this.idOption = idOption;
        this.idQuestion = idQuestion;
        this.option = option;
        this.value = value;
    }

    public int getIdOption() {
        return idOption;
    }

    public void setIdOption(int idOption) {
        this.idOption = idOption;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
