package com.ghani.skripsi.model;

public class QuestionModel {
    int idQuestion;
    String question;

    public QuestionModel() {
    }

    public QuestionModel(int idQuestion, String question) {
        this.idQuestion = idQuestion;
        this.question = question;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
