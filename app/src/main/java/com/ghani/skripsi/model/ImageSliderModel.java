package com.ghani.skripsi.model;

public class ImageSliderModel {
    int imageUrl;

    public ImageSliderModel(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ImageSliderModel() {
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }
}
