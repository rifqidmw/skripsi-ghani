package com.ghani.skripsi.model;

public class GunungModel {
    private int id;
    private String gunung;
    private int foto;
    private String deskripsi;
    private String ketinggian;
    private String startTracking;
    private String koordinat;
    private String provinsi;
    private String jenis;
    private int jalur_pendakian_image;
    private String estimasi;
    private String durasi;
    private String simaksi;
    private int simaksi_image;
    private String fasilitas;
    private String sop;
    private String peralatan;
    private String estimasi_biaya;
    private int kelas;

    public GunungModel() {
    }

    public String getEstimasi_biaya() {
        return estimasi_biaya;
    }

    public void setEstimasi_biaya(String estimasi_biaya) {
        this.estimasi_biaya = estimasi_biaya;
    }

    public String getSop() {
        return sop;
    }

    public void setSop(String sop) {
        this.sop = sop;
    }

    public String getPeralatan() {
        return peralatan;
    }

    public void setPeralatan(String peralatan) {
        this.peralatan = peralatan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGunung() {
        return gunung;
    }

    public void setGunung(String gunung) {
        this.gunung = gunung;
    }

    public int getKelas() {
        return kelas;
    }

    public void setKelas(int kelas) {
        this.kelas = kelas;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getKetinggian() {
        return ketinggian;
    }

    public void setKetinggian(String ketinggian) {
        this.ketinggian = ketinggian;
    }

    public String getStartTracking() {
        return startTracking;
    }

    public void setStartTracking(String startTracking) {
        this.startTracking = startTracking;
    }

    public String getKoordinat() {
        return koordinat;
    }

    public void setKoordinat(String koordinat) {
        this.koordinat = koordinat;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public int getJalur_pendakian_image() {
        return jalur_pendakian_image;
    }

    public void setJalur_pendakian_image(int jalur_pendakian_image) {
        this.jalur_pendakian_image = jalur_pendakian_image;
    }

    public String getEstimasi() {
        return estimasi;
    }

    public void setEstimasi(String estimasi) {
        this.estimasi = estimasi;
    }

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    public String getSimaksi() {
        return simaksi;
    }

    public void setSimaksi(String simaksi) {
        this.simaksi = simaksi;
    }

    public int getSimaksi_image() {
        return simaksi_image;
    }

    public void setSimaksi_image(int simaksi_image) {
        this.simaksi_image = simaksi_image;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }
}
