package com.ghani.skripsi.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.model.ImageSliderModel;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderAdapter extends
        SliderViewAdapter<ImageSliderAdapter.SliderAdapterVH> {
    private Context context;
    private List<ImageSliderModel> mImageSliderModels = new ArrayList<>();

    public ImageSliderAdapter(Context context, List<ImageSliderModel> mImageSliderModels) {
        this.context = context;
        this.mImageSliderModels = mImageSliderModels;
    }

    public void renewItems(List<ImageSliderModel> ImageSliderModels) {
        this.mImageSliderModels = ImageSliderModels;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.mImageSliderModels.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(ImageSliderModel ImageSliderModel) {
        this.mImageSliderModels.add(ImageSliderModel);
        notifyDataSetChanged();
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image_slider, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {

        ImageSliderModel ImageSliderModel = mImageSliderModels.get(position);

        Glide.with(viewHolder.itemView)
                .load(ImageSliderModel.getImageUrl())
                .fitCenter()
                .into(viewHolder.imgGunung);

    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return mImageSliderModels.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imgGunung;

        public SliderAdapterVH(View itemView) {
            super(itemView);
            imgGunung = itemView.findViewById(R.id.img_gunung);
            this.itemView = itemView;
        }
    }
}
