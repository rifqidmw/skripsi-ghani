package com.ghani.skripsi.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ghani.skripsi.R;
import com.ghani.skripsi.model.GunungModel;
import com.ghani.skripsi.model.OptionModel;
import com.ghani.skripsi.model.QuestionModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    // Database name
    private static final String DATABASE_NAME = "db_gunung";

    // table name
    private static final String TABLE_GUNUNG = "gunung";

    // column tables
    private static final String GUNUNG_ID = "id_gunung";
    private static final String GUNUNG_TITLE = "gunung";
    private static final String GUNUNG_FOTO = "foto";
    private static final String GUNUNG_DESKRIPSI = "deskripsi";
    private static final String GUNUNG_KETINGGIAN = "ketinggian";
    private static final String GUNUNG_START_TRACKING = "start_tracking";
    private static final String GUNUNG_KOORDINAT = "koordinat";
    private static final String GUNUNG_PROVINSI = "provinsi";
    private static final String GUNUNG_JENIS = "jenis";
    private static final String GUNUNG_JALUR_PENDAKIAN_IMAGE = "jalur_pendakian_image";
    private static final String GUNUNG_ESTIMASI = "estimasi";
    private static final String GUNUNG_DURASI = "durasi";
    private static final String GUNUNG_SIMAKSI = "simaksi";
    private static final String GUNUNG_SIMAKSI_IMAGE = "simaksi_image";
    private static final String GUNUNG_FASILITAS = "fasilitas";
    private static final String GUNUNG_SOP = "sop";
    private static final String GUNUNG_PERALATAN = "peralatan";
    private static final String GUNUNG_ESTIMASI_BIAYA = "estimasi_biaya";
    private static final String GUNUNG_BOBOT = "bobot";

    public DatabaseHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_GUNUNG_TABLE = "CREATE TABLE " + TABLE_GUNUNG + "("
                + GUNUNG_ID + " INTEGER PRIMARY KEY," + GUNUNG_TITLE + " TEXT, " + GUNUNG_FOTO + " INTEGER," + GUNUNG_DESKRIPSI + " TEXT,"
                + GUNUNG_KETINGGIAN + " TEXT," + GUNUNG_START_TRACKING + " TEXT," + GUNUNG_KOORDINAT + " TEXT," + GUNUNG_PROVINSI + " TEXT,"
                + GUNUNG_JENIS + " TEXT," + GUNUNG_JALUR_PENDAKIAN_IMAGE + " INTEGER," + GUNUNG_ESTIMASI + " TEXT," + GUNUNG_DURASI + " TEXT,"
                + GUNUNG_SIMAKSI + " TEXT," + GUNUNG_SIMAKSI_IMAGE + " INTEGER," + GUNUNG_FASILITAS + " TEXT," + GUNUNG_SOP + " TEXT,"
                + GUNUNG_PERALATAN + " TEXT," + GUNUNG_ESTIMASI_BIAYA + " TEXT,"  + GUNUNG_BOBOT + " INTEGER" + ")";
        db.execSQL(CREATE_GUNUNG_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GUNUNG);
        onCreate(db);
    }

    public List<GunungModel> getGunung(int bobot, int limit) {
        List<GunungModel> gunungList = new ArrayList<GunungModel>();
        // Select All Query
        String selectQuery = "SELECT * FROM "+TABLE_GUNUNG+" WHERE "+GUNUNG_BOBOT+" = "+bobot+" LIMIT "+limit;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                GunungModel gunungModel = new GunungModel();
                gunungModel.setId(Integer.parseInt(cursor.getString(0)));
                gunungModel.setGunung(cursor.getString(1));
                gunungModel.setFoto(Integer.parseInt(cursor.getString(2)));
                gunungModel.setDeskripsi(cursor.getString(3));
                gunungModel.setKetinggian(cursor.getString(4));
                gunungModel.setStartTracking(cursor.getString(5));
                gunungModel.setKoordinat(cursor.getString(6));
                gunungModel.setProvinsi(cursor.getString(7));
                gunungModel.setJenis(cursor.getString(8));
                gunungModel.setJalur_pendakian_image(Integer.parseInt(cursor.getString(9)));
                gunungModel.setEstimasi(cursor.getString(10));
                gunungModel.setDurasi(cursor.getString(11));
                gunungModel.setSimaksi(cursor.getString(12));
                gunungModel.setSimaksi_image(Integer.parseInt(cursor.getString(13)));
                gunungModel.setFasilitas(cursor.getString(14));
                gunungModel.setSop(cursor.getString(15));
                gunungModel.setPeralatan(cursor.getString(16));
                gunungModel.setEstimasi_biaya(cursor.getString(17));
                gunungModel.setKelas(Integer.parseInt(cursor.getString(18)));

                gunungList.add(gunungModel);
            } while (cursor.moveToNext());
        }

        // return gunung list
        return gunungList;
    }

    public GunungModel getGunungByName(String title) {
        // Select All Query
        String selectQuery = "SELECT * FROM "+TABLE_GUNUNG+" WHERE "+GUNUNG_TITLE+" = '"+title+"'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();

        GunungModel gunungModel = new GunungModel();
        gunungModel.setId(Integer.parseInt(cursor.getString(0)));
        gunungModel.setGunung(cursor.getString(1));
        gunungModel.setFoto(Integer.parseInt(cursor.getString(2)));
        gunungModel.setDeskripsi(cursor.getString(3));
        gunungModel.setKetinggian(cursor.getString(4));
        gunungModel.setStartTracking(cursor.getString(5));
        gunungModel.setKoordinat(cursor.getString(6));
        gunungModel.setProvinsi(cursor.getString(7));
        gunungModel.setJenis(cursor.getString(8));
        gunungModel.setJalur_pendakian_image(Integer.parseInt(cursor.getString(9)));
        gunungModel.setEstimasi(cursor.getString(10));
        gunungModel.setDurasi(cursor.getString(11));
        gunungModel.setSimaksi(cursor.getString(12));
        gunungModel.setSimaksi_image(Integer.parseInt(cursor.getString(13)));
        gunungModel.setFasilitas(cursor.getString(14));
        gunungModel.setSop(cursor.getString(15));
        gunungModel.setPeralatan(cursor.getString(16));
        gunungModel.setEstimasi_biaya(cursor.getString(17));
        gunungModel.setKelas(Integer.parseInt(cursor.getString(18)));

        // return detail gunung
        return gunungModel;
    }

    public GunungModel getDetailGunung(int id) {
        // Select All Query
        String selectQuery = "SELECT * FROM "+TABLE_GUNUNG+" WHERE "+GUNUNG_ID+" = "+id+" LIMIT 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();

        GunungModel gunungModel = new GunungModel();
        gunungModel.setId(Integer.parseInt(cursor.getString(0)));
        gunungModel.setGunung(cursor.getString(1));
        gunungModel.setFoto(Integer.parseInt(cursor.getString(2)));
        gunungModel.setDeskripsi(cursor.getString(3));
        gunungModel.setKetinggian(cursor.getString(4));
        gunungModel.setStartTracking(cursor.getString(5));
        gunungModel.setKoordinat(cursor.getString(6));
        gunungModel.setProvinsi(cursor.getString(7));
        gunungModel.setJenis(cursor.getString(8));
        gunungModel.setJalur_pendakian_image(Integer.parseInt(cursor.getString(9)));
        gunungModel.setEstimasi(cursor.getString(10));
        gunungModel.setDurasi(cursor.getString(11));
        gunungModel.setSimaksi(cursor.getString(12));
        gunungModel.setSimaksi_image(Integer.parseInt(cursor.getString(13)));
        gunungModel.setFasilitas(cursor.getString(14));
        gunungModel.setSop(cursor.getString(15));
        gunungModel.setPeralatan(cursor.getString(16));
        gunungModel.setEstimasi_biaya(cursor.getString(17));
        gunungModel.setKelas(Integer.parseInt(cursor.getString(18)));

        // return detail gunung
        return gunungModel;
    }

    public void removeTable(){
        SQLiteDatabase db = getWritableDatabase();

        db.execSQL("DROP TABLE db_gunung");
    }

    public void addGunung(){
        SQLiteDatabase db  = getWritableDatabase();

        ContentValues gunung1 = new ContentValues();
        gunung1.put(GUNUNG_ID, 1);
        gunung1.put(GUNUNG_TITLE, "Merbabu");
        gunung1.put(GUNUNG_FOTO, R.drawable.ic_merbabu);
        gunung1.put(GUNUNG_DESKRIPSI, "Adalah  gunung api yang bertipe Strato (lihat Gunung Berapi) yang terletak secara geografis pada 7,5° LS dan 110,4° BT. Secara administratif gunung ini berada di wilayah Kabupaten Magelang di lereng sebelah barat dan Kabupaten Boyolali di lereng sebelah timur dan selatan, Kabupaten Semarang di lereng sebelah utara, Provinsi Jawa Tengah.");
        gunung1.put(GUNUNG_KETINGGIAN, "3.145 mdpl (10.630 kaki)");
        gunung1.put(GUNUNG_START_TRACKING, "Basecamp Selo");
        gunung1.put(GUNUNG_KOORDINAT, "7.5°S 110.4°E");
        gunung1.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung1.put(GUNUNG_JENIS, "Stratovolcano");
        gunung1.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_merbabu);
        gunung1.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos 1 : 2,5 jam\n" +
                "•\tPos 1 – Pos 2 : 30 menit\n" +
                "•\tPos 2 – Pos 3 : 1 jam\n" +
                "•\tPos 3 – Sabana 1 : 2 jam\n" +
                "•\tSabana 1 – Sabana 2 : 1 jam\n" +
                "•\tSabana 2 – Puncak Kenteng Songo : 2 jam\n" +
                "•\tTotal Perjalanan : 9 jam berjalan santai\n");
        gunung1.put(GUNUNG_DURASI, "");
        gunung1.put(GUNUNG_SIMAKSI, "");
        gunung1.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung1.put(GUNUNG_FASILITAS, "Fasilitas dan pelayanan yang ada di jalur pendakian Selo diantaranya adalah Pos pemungutan retribusi PNBP dan basecamp yang dikelola oleh warga. Di situ juga digunakan sebagai tempat penjualan berbagai macam souvenir (gantungan kunci, kaos, stiker, slayer,dll), sewa alat-alat pendakian / sewa alat outdoor. Di basecamp juga menyediakan berbagai jenis makanan (soto, mie goreng / mie rebus, nasi rames), aneka minuman (kopi, teh, susu dan lain-lain).");
        gunung1.put(GUNUNG_SOP, "•\tMelakukan boking online untuk pemesanan tiket pendakian.\n" +
                "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung1.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung1.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi  : Rp, 5.000/ perorang\n" +
                "•\tParkir : Rp, 5.000/permotor\n");
        gunung1.put(GUNUNG_BOBOT, 10);
        db.insert(TABLE_GUNUNG, null, gunung1);

        ContentValues gunung2 = new ContentValues();
        gunung2.put(GUNUNG_ID, 2);
        gunung2.put(GUNUNG_TITLE, "Merapi");
        gunung2.put(GUNUNG_FOTO, R.drawable.ic_merapi);
        gunung2.put(GUNUNG_DESKRIPSI, "Gunung Merapi adalah gunung berapi di bagian tengah Pulau Jawa dan merupakan salah satu gunung api teraktif di Indonesia. Posisi geografis kawasan Taman Nasional Gunung Merapi adalah di antara koordinat 07°22'33\" - 07°52'30\" LS dan 110°15'00\" - 110°37'30\" BT. Sedangkan luas totalnya sekitar 6.410 ha, dengan 5.126,01 ha di wilayah Jawa Tengah dan 1.283,99 ha di Daerah Istimewa Yogyakarta. Secara administratif lereng sisi selatan berada dalam administrasi Kabupaten Sleman, Daerah Istimewa Yogyakarta, dan sisanya berada dalam wilayah Provinsi Jawa Tengah, yaitu Kabupaten Magelang di sisi barat, Kabupaten Boyolali di sisi utara dan timur, serta Kabupaten Klaten di sisi tenggara. Kawasan hutan di sekitar puncaknya menjadi kawasan Taman Nasional Gunung Merapi sejak tahun 2004.");
        gunung2.put(GUNUNG_KETINGGIAN, "2.930 mdpl per 2010");
        gunung2.put(GUNUNG_START_TRACKING, "Basecamp New Selo");
        gunung2.put(GUNUNG_KOORDINAT, "07°22'33\" - 07°52'30\" LS dan 110°15'00\" - 110°37'30\" BT");
        gunung2.put(GUNUNG_PROVINSI, "Daerah Istimewa Yogyakarta");
        gunung2.put(GUNUNG_JENIS, "Gunung berapi kerucut");
        gunung2.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_merapi);
        gunung2.put(GUNUNG_ESTIMASI, "•\tBasecamp – New Selo (15 menit)\n" +
                "•\tNew Selo – Pos I (120 Menit)\n" +
                "•\tPos 1 – Pos 2 (120 Menit)\n" +
                "•\tPos 2 – Watu Gajah (60 Menit)\n" +
                "•\tWatu Gajah – Pasar Bubrah (30 Menit)\n" +
                "•\tPasar Bubrah – Puncak Gunung Merapi (60 Menit)\n");
        gunung2.put(GUNUNG_DURASI, "Normal 2-3 hari pendakian");
        gunung2.put(GUNUNG_SIMAKSI, "Biaya karcis Masuk : Rp 5.000,00 per orang\n" +
                "Biaya tracking dan pendakian : Rp 5.000,00 per orang\n" +
                "Biaya karcis jasa jalur Gunung Merapi : Rp. 5.000,00 per orang\n" +
                "Total Biaya Rp.15.000,00 per orang\n");
        gunung2.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung2.put(GUNUNG_FASILITAS, "Fasilitas yang sudah tersedia antara lain basecamp, spot foto, toilet, dan tempat parkir. ");
        gunung2.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang melewati zona aman pendakian.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung2.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung2.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk : Rp, 5.000/orang\n" +
                "•\tParkir : Rp, 5.000/permotor\n");
        gunung2.put(GUNUNG_BOBOT, 8);
        db.insert(TABLE_GUNUNG, null, gunung2);

        ContentValues gunung3 = new ContentValues();
        gunung3.put(GUNUNG_ID, 3);
        gunung3.put(GUNUNG_TITLE, "Sindoro");
        gunung3.put(GUNUNG_FOTO, R.drawable.ic_sindoro);
        gunung3.put(GUNUNG_DESKRIPSI, "Gunung Sindoro, biasa disebut Sindara, atau juga Sundoro memiliki merupakan gunung berapi berbentuk kerucut dengan tipe Strato,  ketinggian  3.150 mdpl di batas Kabupaten Temanggung sebelah barat dan sebelah timur kota Wonosobo dengan koordinat/ geografi pada 7° 18' 00\" LS dan 109° 59' 30\"BT. Gunung Sindoro terletak berdampingan dengan Gunung Sumbing.Gunung sindara dapat terlihat jelas dari puncak sikunir dieng");
        gunung3.put(GUNUNG_KETINGGIAN, "3.150 mdpl");
        gunung3.put(GUNUNG_START_TRACKING, "Basecamp Kledung");
        gunung3.put(GUNUNG_KOORDINAT, "7° 18' 00\" LS dan 109° 59' 30\"BT");
        gunung3.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung3.put(GUNUNG_JENIS, "Gunung Stratovolcano");
        gunung3.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_sindoro);
        gunung3.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos I : ± 25 menit dengan ojek, ± 1 jam dengan jalan kaki\n" +
                "•\tPos I – Pos II : ± 1 jam\n" +
                "•\tPos II – Pos III : ± 1,5 jam\n" +
                "•\tPos III – Pos IV : ± 1,5 jam\n" +
                "•\tPos IV – Puncak : ± 45 menit\n" +
                "•\tTotal Estimasi Waktu Pendakian : ± 7 – 8 jam\n");
        gunung3.put(GUNUNG_DURASI, "Normal 2-3 hari");
        gunung3.put(GUNUNG_SIMAKSI, "Tiket Masuk : Rp, 15.000,00 perorang\n" +
                "Parkir : Rp, 5.000,00 permotor\n" +
                "Jasa ojek dari basecamp – Pos 1 : Rp,25.000,00 perorang\n");
        gunung3.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung3.put(GUNUNG_FASILITAS, "Base camp Kledung Gunung Sindoro memiliki fasilitas, loket registrasi, markas tim SAR Grasindo, aula sebagai tempat istirahat, tempat penitipan barang, toilet umum, hingga area parkir. Tak jauh dari base camp, pengunjung juga bisa menemukan masjid dan warung.");
        gunung3.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung3.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung3.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk : Rp, 15.000/orang\n" +
                "•\tParkir : Rp, 5.000/permotor\n" +
                "•\tJasa ojek dari basecamp – Pos 1 : Rp,25.000/orang\n");
        gunung3.put(GUNUNG_BOBOT, 11);
        db.insert(TABLE_GUNUNG, null, gunung3);

        ContentValues gunung4 = new ContentValues();
        gunung4.put(GUNUNG_ID, 4);
        gunung4.put(GUNUNG_TITLE, "Sumbing");
        gunung4.put(GUNUNG_FOTO, R.drawable.ic_sumbing);
        gunung4.put(GUNUNG_DESKRIPSI, "Gunung Sumbing adalah  gunung api yang bertipe Strato tipe A yang terletak secara geografis pada 7°23’ LS dan 110°03’30” BT. Secara administratif gunung ini berada di sebelah tenggara kota magelang, di sebelah barat daya kota temanggung , dan di sebelah timur kota Wonosobo Provinsi Jawa Tengah.");
        gunung4.put(GUNUNG_KETINGGIAN, "3.371 mdpl");
        gunung4.put(GUNUNG_START_TRACKING, "Basecamp Garung");
        gunung4.put(GUNUNG_KOORDINAT, "7°23’ LS dan 110°03’30”");
        gunung4.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung4.put(GUNUNG_JENIS, "Stratovolcano");
        gunung4.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_sumbing);
        gunung4.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos 1 malim : 15 menit menggunakan Ojek / 3 jam jika jalan kaki.\n" +
                "•\tPos 1 Malim – Pos 2 Gatakan : 3 Jam\n" +
                "•\tPos 2 Gatakan – Pestan : 15 menit\n" +
                "•\tPestan – Pasar Watu : 1,5 Jam\n" +
                "•\tPasar Watu – Watu Kotak : 2 Jam\n" +
                "•\tWatu Kotak – Puncak : 1 jam\n");
        gunung4.put(GUNUNG_DURASI, "Normal 2-3 Hari");
        gunung4.put(GUNUNG_SIMAKSI, "Rp. 15.000,00 /orang\n" +
                "  Parkir : Rp, 5.000,00 permotor\n" +
                "  Jasa ojek dari basecamp – Pos 1 : Rp,25.000,00 perorang\n");
        gunung4.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung4.put(GUNUNG_FASILITAS, "Loket registrasi, mushola, WC atau toilet, lahan parkir, tempat peristirahatan, TV, warung atau kantin, tempat sampah.");
        gunung4.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMelapor setelah pendakian.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n");
        gunung4.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic/makanan \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung4.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk : Rp, 15.000,00 perorang\n" +
                "•\tParkir : Rp, 5.000,00 permotor\n" +
                "•\tJasa ojek dari basecamp – Pos 1 : Rp,35.000,00 perorang\n");
        gunung4.put(GUNUNG_BOBOT, 9);
        db.insert(TABLE_GUNUNG, null, gunung4);

        ContentValues gunung5 = new ContentValues();
        gunung5.put(GUNUNG_ID, 5);
        gunung5.put(GUNUNG_TITLE, "Andong");
        gunung5.put(GUNUNG_FOTO, R.drawable.ic_andong);
        gunung5.put(GUNUNG_DESKRIPSI, "Gunung Andong adalah gunung tipe perisai yang terletak di Kabupaten Magelang, berbatasan langsung dengan Kabupaten Semarang dan Kota Salatiga, Provinsi Jawa Tengah. Berketinggian 1.726 mdpl dengan titik kordinat 7°23’24.19”S 110°22’13.25” E.");
        gunung5.put(GUNUNG_KETINGGIAN, "1.726 mdpl");
        gunung5.put(GUNUNG_START_TRACKING, "Basecamp Sawit");
        gunung5.put(GUNUNG_KOORDINAT, "7°23’24.19”S 110°22’13.25” E.");
        gunung5.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung5.put(GUNUNG_JENIS, "Perisai");
        gunung5.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_andong);
        gunung5.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos 1 : 20 menit\n" +
                "•\tPos 1 – Pos 2 : 20 menit\n" +
                "•\tPos 2 – Puncak Jiwa : 30 menit\n" +
                "•\tPuncak Jiwa – Puncak Andong : 5 menit\n");
        gunung5.put(GUNUNG_DURASI, "Normal 1 hari");
        gunung5.put(GUNUNG_SIMAKSI, "Rp,8.000,00 perorang\n" +
                "  Parkir : Rp, 5.000,00 permotor\n");
        gunung5.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung5.put(GUNUNG_FASILITAS, "Fasilitas pendukung Basecamp Sawit cukup lengkap mulai dari area parkir, warung kelontong, warung makan 24 jam, kamar mandi umum, hingga tempat peristirahatan yang disediakan berupa lantai yang dilapisi karpet. Selain itu saat ini Basecamp Taruna Jayagiri melayani persewaan alat-alat camping mulai dari tenda, sleepingbag, senter, dan sebagainya. ");
        gunung5.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang menyalakan kembang api.\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung5.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");

        gunung5.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi : Rp, 8.000/orang\n" +
                "•\tParkir : Rp, 5.000/motor\n");
        gunung5.put(GUNUNG_BOBOT, 4);
        db.insert(TABLE_GUNUNG, null, gunung5);

        ContentValues gunung7 = new ContentValues();
        gunung7.put(GUNUNG_ID, 7);
        gunung7.put(GUNUNG_TITLE, "Ungaran");
        gunung7.put(GUNUNG_FOTO, R.drawable.ic_ungaran);
        gunung7.put(GUNUNG_DESKRIPSI, "Gunung Ungaran  adalah gunung berapi bertipe Stratovolcano yang terletak di Ungaran, Kabupaten Semarang, Jawa Tengah dan memiliki ketinggian 2.050 mdpl dengan titik koordinat110°20’27” BT dan 07°14’3” LS.");
        gunung7.put(GUNUNG_KETINGGIAN, "2050 mdpl");
        gunung7.put(GUNUNG_START_TRACKING, "Basecamp Mawar");
        gunung7.put(GUNUNG_KOORDINAT, "110°20’27” BT dan 07°14’3” LS");
        gunung7.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung7.put(GUNUNG_JENIS, "Stratovolcano");
        gunung7.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_ungaran);
        gunung7.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos 1 \t: 15 menit\n" +
                "•\tPos 1 – Pos 2 \t\t: 25 menit\n" +
                "•\tPos 2 – Pos 3\t\t: 30 menit\n" +
                "•\tPos 3 – Kebun Teh\t: 15 menit\n" +
                "•\tKebun The – Puncak \t: 1 jam 30 menit\n");
        gunung7.put(GUNUNG_DURASI, "Normal 1 hari");
        gunung7.put(GUNUNG_SIMAKSI, "Rp.15.000/orang");
        gunung7.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung7.put(GUNUNG_FASILITAS, "Area parkir, warung kelontong, warung makan, kamar mandi umum, hingga tempat peristirahatan, camping ground, dan melayani 24 jam.");
        gunung7.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang menyalakan kembang api.\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung7.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung7.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi : Rp, 7.000/orang\n" +
                "•\tParkir : Rp, 5.000/motor\n");
        gunung7.put(GUNUNG_BOBOT, 5);
        db.insert(TABLE_GUNUNG, null, gunung7);

        ContentValues gunung8 = new ContentValues();
        gunung8.put(GUNUNG_ID, 8);
        gunung8.put(GUNUNG_TITLE, "Kembang");
        gunung8.put(GUNUNG_FOTO, R.drawable.ic_kembang);
        gunung8.put(GUNUNG_DESKRIPSI, "Gunung Kembang sering disebut sebagai anak Gunung Sindoro. Memiliki ketinggian 2.320 meter di atas permukaan laut (mdpl) merupakan salah satu gunung yang terletak di Kabupaten Wonosobo, Jawa Tengah, tepatnya di Desa Blembem Damarkasihan, Kecamatan Kretek. Titik kordinat gunung kembang yaitu 7°20'08.3\"S 109°58'05.6\"E.");
        gunung8.put(GUNUNG_KETINGGIAN, "2.320 mdpl");
        gunung8.put(GUNUNG_START_TRACKING, "Basecamp Blembem");
        gunung8.put(GUNUNG_KOORDINAT, "7°20'08.3\"S 109°58'05.6\"E");
        gunung8.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung8.put(GUNUNG_JENIS, "Perisai");
        gunung8.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_kembang);
        gunung8.put(GUNUNG_ESTIMASI, "•\tBasecamp - Istana Katak : 40 menit\n" +
                "•\tIstana Katak – Kandang Celeng : 15 menit\n" +
                "•\tKandang Celeng – Pos 1 Liliput : 30 menit\n" +
                "•\tPos 1 Liliput – Pos 2 Simpang 3 : 20 menit\n" +
                "•\tPos 2 Simpang 3 – Pos 3 Akar :15 menit\n" +
                "•\tPos 3 Akar – Pos 4 Sabana : 50 menit\n" +
                "•\tPos 4 Sabana – Pos 4 Tanjakan Mesra : 10 menit\n" +
                "•\tPos 4 Tanjakan Mesra – Geger Celeng : 30 menit\n" +
                "•\tGeger Celeng – Puncak Gunung Kemban : 5 menit\n");
        gunung8.put(GUNUNG_DURASI, "Normal 1-2 hari");
        gunung8.put(GUNUNG_SIMAKSI, "Rp, 15.000,00 perorang");
        gunung8.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung8.put(GUNUNG_FASILITAS, "Loket Retribusi, toilet umum, air bersih, menyediakan juga kebutuhan konsumsi, sewa peralatan pendakian dan souvenir khas Gunung Kembang. ");
        gunung8.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang menyalakan kembang api.\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung8.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung8.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi : Rp, 15.000/orang\n" +
                "•\tBiaya perawatan basecamp : Rp, 5.000/orang\n" +
                "•\tParkir : Rp, 5.000/motor\n");
        gunung8.put(GUNUNG_BOBOT, 6);
        db.insert(TABLE_GUNUNG, null, gunung8);

        ContentValues gunung9 = new ContentValues();
        gunung9.put(GUNUNG_ID, 9);
        gunung9.put(GUNUNG_TITLE, "Prau");
        gunung9.put(GUNUNG_FOTO, R.drawable.ic_prau);
        gunung9.put(GUNUNG_DESKRIPSI, "Gunung Prahu (terkadang dieja Gunung Prau) (2.565 mdpl) terletak di kawasan Dataran Tinggi Dieng, Jawa Tengah, Indonesia. Gunung Prahu terletak pada koordinat 7°11′13″S 109°55′22″E. Gunung Prahu merupakan gunung kecil bukan api yang berbatasan antara tiga kabupaten yaitu Kabupaten Batang, Kabupaten Kendal dan Kabupaten Wonosobo.");
        gunung9.put(GUNUNG_KETINGGIAN, "2.565 mdpl");
        gunung9.put(GUNUNG_START_TRACKING, "Basecamp Patak Banteng");
        gunung9.put(GUNUNG_KOORDINAT, "7°11′13″S 109°55′22″E");
        gunung9.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung9.put(GUNUNG_JENIS, "Gunung kecil bukan api");
        gunung9.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_prau);
        gunung9.put(GUNUNG_ESTIMASI, "•\tBasecamp – Pos 1                : ± 20 menit\n" +
                "•\tPos 1 – Pos 2 : ± 20 menit\n" +
                "•\tPos 2 – Pos 3 : ± 30 menit\n" +
                "•\tPos 3 – Puncak : ± 30 menit\n" +
                "•\tTotal Estimasi Waktu Pendakian : ± 1,5 – 2 jam\n");
        gunung9.put(GUNUNG_DURASI, "Normal 1-2 hari");
        gunung9.put(GUNUNG_SIMAKSI, "Rp, 15.000,00 perorang");
        gunung9.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung9.put(GUNUNG_FASILITAS, "Area parkir, warung kelontong, warung makan 24 jam, kamar mandi umum, hingga tempat peristirahatan yang disediakan berupa lantai yang dilapisi karpet. Selain itu basecamp patak banteng juga melayani persewaan alat-alat camping mulai dari tenda, sleepingbag, senter, dan sebagainya. ");
        gunung9.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelakukan cek list peralatan yang dibawa dan diperiksa oleh petugas.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang menyalakan kembang api.\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung9.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung9.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi : Rp, 15.000/orang\n" +
                "•\tParkir : Rp, 5.000/permotor\n");
        gunung9.put(GUNUNG_BOBOT, 7);
        db.insert(TABLE_GUNUNG, null, gunung9);

        ContentValues gunung14 = new ContentValues();
        gunung14.put(GUNUNG_ID, 14);
        gunung14.put(GUNUNG_TITLE, "Slamet");
        gunung14.put(GUNUNG_FOTO, R.drawable.ic_slamet);
        gunung14.put(GUNUNG_DESKRIPSI, "Gunung Slamet (3.428 meter dpl.) adalah sebuah gunung berapi kerucut yang terdapat di Pulau Jawa, Indonesia. Posisi geografis Gunung Slamet: 7°14,30' LS dan 109°12,30' BT. Gunung Slamet terletak di antara 5 kabupaten, yaitu Kabupaten Brebes, Kabupaten Banyumas, Kabupaten Purbalingga, Kabupaten Tegal, dan Kabupaten Pemalang, Provinsi Jawa Tengah. Gunung Slamet merupakan gunung tertinggi di Jawa Tengah serta kedua tertinggi di Pulau Jawa setelah Gunung Semeru. Kawah IV merupakan kawah terakhir yang masih aktif sampai sekarang, dan terakhir aktif hingga pada level siaga medio-2009.");
        gunung14.put(GUNUNG_KETINGGIAN, "3.428 mdpl");
        gunung14.put(GUNUNG_START_TRACKING, "Basecamp Bambangan");
        gunung14.put(GUNUNG_KOORDINAT, "7°14,30' LS dan 109°12,30' BT");
        gunung14.put(GUNUNG_PROVINSI, "Jawa Tengah");
        gunung14.put(GUNUNG_JENIS, "Stratovolcano");
        gunung14.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, R.drawable.ic_jalur_slamet);
        gunung14.put(GUNUNG_ESTIMASI, "•\tBASECAMP – POS I Pondok Gembirung (1jam)\n" +
                "•\tPOS I – POS II Pondok Walang (1,5jam)\n" +
                "•\tPOS II – POS III Pondok Cemara (1jam)\n" +
                "•\tPOS III – POS IV Pondok Samaranthu (1jam)\n" +
                "•\tPOS IV – POS V Samyang Rangkah (30 menit)\n" +
                "•\tPOS V – POS VI Samyang Jampang (30 menit)\n" +
                "•\tPOS VI – POS VII Samyang Kendil (30 menit)\n" +
                "•\tPOS VII – POS VIII – Puncak Slamet/ Bibir Kawah (2jam)\n" +
                "•\tMemutar puncak bibir kawah (30 menit)\n" +
                "•\tTOTAL = 8,5 jam\n");
        gunung14.put(GUNUNG_DURASI, "Normal 2-3 hari");
        gunung14.put(GUNUNG_SIMAKSI, "Rp, 25.000,00 perorang");
        gunung14.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung14.put(GUNUNG_FASILITAS, "Loket retribusi, tempat parker, tempat peristirahatan yang luas, toilet umum. Di sekitar basecamp juga menjual makanan nasi sayur telor dan juga teh anget. Serta ada toko suvenir dan keperluan mendaki.");
        gunung14.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk dan asuransi\n" +
                "•\tMelapor kepada petugas sebelum dan setelah kunjungan pendakian.\n" +
                "•\tDengan ikhlas siap diperiksa dan mencatat barang perbekalan untuk pendakian oleh petugas.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalism dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n");
        gunung14.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic/makanan \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\t P3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung14.put(GUNUNG_ESTIMASI_BIAYA, "•\tRp.35.000/orang (biaya angkot)\n" +
                "•\tRp. 5.000/orang (biaya tiket masuk)\n");
        gunung14.put(GUNUNG_BOBOT, 12);
        db.insert(TABLE_GUNUNG, null, gunung14);

        ContentValues gunung15 = new ContentValues();
        gunung15.put(GUNUNG_ID, 15);
        gunung15.put(GUNUNG_TITLE, "Langeran");
        gunung15.put(GUNUNG_FOTO, R.drawable.ic_langeran);
        gunung15.put(GUNUNG_DESKRIPSI, "Gunung Nglanggeran adalah sebuah gunung di Daerah Istimewa Yogyakarta, Indonesia. Gunung ini merupakan suatu gunung api purba yang terbentuk sekitar 0,6-70 juta tahun yang lalu atau yang memiliki umur tersier (Oligo-Miosen). Puncak gunung tersebut adalah Gunung Gedhe di ketinggian sekitar 700 meter dari permukaan laut, dengan luas kawasan pegunungan mencapai 48 hektar. ");
        gunung15.put(GUNUNG_KETINGGIAN, "700 mdpl");
        gunung15.put(GUNUNG_START_TRACKING, "Desa Nglanggeran");
        gunung15.put(GUNUNG_KOORDINAT, "7° 50,4′ 25” LS dan 110° 32,3′ 21” BT");
        gunung15.put(GUNUNG_PROVINSI, "DI Yogyakarta");
        gunung15.put(GUNUNG_JENIS, "");
        gunung15.put(GUNUNG_JALUR_PENDAKIAN_IMAGE, 0);
        gunung15.put(GUNUNG_ESTIMASI, "1,5 – 2 Jam pendakian");
        gunung15.put(GUNUNG_DURASI, "Normal 1 hari");
        gunung15.put(GUNUNG_SIMAKSI, "Tiket masuk untuk wisatawan domestik Rp15.000 (siang), Rp20.000 (malam), sementara untuk wisatawan mancanegara Rp30.000 (siang malam).");
        gunung15.put(GUNUNG_SIMAKSI_IMAGE, 0);
        gunung15.put(GUNUNG_FASILITAS, "Tersedia warung makan, tempat istirahat, mushola dan kamar mandi, tempat parkir, tempat foto.");
        gunung15.put(GUNUNG_SOP, "•\tMemiliki surat ijin masuk/ tiket resmi.\n" +
                "•\tMeninggalkan kartu identitas.\n" +
                "•\tMelengkapi peralatan pendakian.\n" +
                "•\tMembawa perbekalan logistik yang cukup.\n" +
                "•\tMelapor sebelum dan setelah pendakian.\n" +
                "•\tMenjaga kearifan lokal.\n" +
                "•\tDilarang merusak dan mengambil flora/fauna \n" +
                "•\tDilarang merusak fasilitas yang ada pada kawasan.\n" +
                "•\tDilarang melakukan aksi vandalisme dalam kawasan.\n" +
                "•\tDilarang membuang sampah apapun didalam kawasan (bawalah sampah kembali turun).\n" +
                "•\tIkut menjaga dan mewaspadai terjadinya kebakaran hutan.\n" +
                "•\tDilarang membawa senjata api dan senjata tajam yang melebihi batas ukuran (15cm).\n" +
                "•\tDilarang membawa alat-alat yang menyebabkan kegaduhan dan suara yang mengganggu (Radio,Tape,Gitar).\n" +
                "•\tDilarang menyalakan kembang api.\n" +
                "•\tDilarang membawa obat-obatan terlarang “NARKOBA” dan minuman keras.\n" +
                "•\tBerdoa sebelum memulai pendakian.\n");
        gunung15.put(GUNUNG_PERALATAN, "•\tTenda dome sesuai kapasitas\n" +
                "•\tLogistic(makanan dan minuman) \n" +
                "•\tPeralatan komunikasi (handy talkie)\n" +
                "•\tPeralatan masak (kompor, nesting, gas, korek api, spirtus)\n" +
                "•\tPeralatan makan (piring makan,sendok,garpu,gelas “bahan plastik”)\n" +
                "•\tPeralatan mandi (sikat gigi,pasta gigi ,dll)\n" +
                "•\tTas gunung/carrier\n" +
                "•\tJaket gunung\n" +
                "•\tSepatu dan sandal gunung\n" +
                "•\tSleeping bag\n" +
                "•\tPakaian outdoor (baju, celana,baju dalaman,kaos kaki,sarung tangan, kupluk/topi,syal,masker)\n" +
                "•\tMatras\n" +
                "•\tPenerangan (senter dan headlamp)\n" +
                "•\tTongkat gunung/tracking pole\n" +
                "•\tKacamata dan jam tangan\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tAlat dokumentasi (kamera,gopro,dll)\n" +
                "•\tJas hujan/raincoat dan cover bag\n" +
                "•\tP3K\n" +
                "•\tAlat navigasi (kompas dan GPS)\n");
        gunung15.put(GUNUNG_ESTIMASI_BIAYA, "•\tTiket Masuk/Asuransi : Rp, 15.000/orang\n" +
                "•\tParkir : Rp, 5.000/motor\n");
        gunung15.put(GUNUNG_BOBOT, 3);
        db.insert(TABLE_GUNUNG, null, gunung15);

        db.close();
    }
}
