package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class SopActivity extends AppCompatActivity {
    DatabaseHandler dbHandler;
    ImageView imgGunung;
    TextView tvSop;
    Button btnMengerti;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sop);

        imgGunung = findViewById(R.id.img_gunung);
        tvSop = findViewById(R.id.tv_sop);
        btnMengerti = findViewById(R.id.btn_mengerti);

        dbHandler = new DatabaseHandler(SopActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        Glide.with(SopActivity.this).load(gunungModel.getFoto()).into(imgGunung);

        if (!gunungModel.getSop().equals("")){
            tvSop.setText(gunungModel.getSop());
        } else {
            tvSop.setText("S.O.P tidak tersedia");
        }

        btnMengerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}