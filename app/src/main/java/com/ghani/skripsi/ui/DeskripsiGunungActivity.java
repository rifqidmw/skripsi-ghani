package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class DeskripsiGunungActivity extends AppCompatActivity {

    ImageView imgGunung;
    TextView tvNamaGunung, tvProvinsi, tvKoordinat, tvJenis, tvKetinggian, tvDeskripsi;
    Button btnDetail;
    DatabaseHandler dbHandler;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deskripsi_gunung);

        imgGunung = findViewById(R.id.img_gunung);
        tvNamaGunung = findViewById(R.id.tv_nama_gunung);
        tvProvinsi = findViewById(R.id.tv_provinsi);
        tvKoordinat = findViewById(R.id.tv_koordinat);
        tvJenis = findViewById(R.id.tv_ketinggian);
        tvDeskripsi = findViewById(R.id.tv_deskripsi);
        btnDetail = findViewById(R.id.btn_detail);

        dbHandler = new DatabaseHandler(DeskripsiGunungActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        Glide.with(DeskripsiGunungActivity.this).load(gunungModel.getFoto()).into(imgGunung);
        tvNamaGunung.setText("Gunung: "+gunungModel.getGunung());
        tvProvinsi.setText("Provinsi: "+gunungModel.getProvinsi());
        tvKoordinat.setText("Koordinat: "+gunungModel.getKoordinat());
        tvJenis.setText("Jenis Gunung: "+gunungModel.getJenis());
        StringBuffer sb1 = new StringBuffer();
        sb1.append("Deskripsi: "+ "\n" + gunungModel.getDeskripsi());
        tvDeskripsi.setText(sb1);

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DeskripsiGunungActivity.this, DetailActivity.class);
                intent.putExtra("ID_GUNUNG", gunungModel.getId());
                startActivity(intent);
            }
        });
    }
}