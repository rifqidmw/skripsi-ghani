package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Notification;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.appyvet.materialrangebar.RangeBar;
import com.ghani.skripsi.R;
import com.ghani.skripsi.adapter.ImageSliderAdapter;
import com.ghani.skripsi.model.ImageSliderModel;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class RecommendationActivity extends AppCompatActivity {

    int valueJarakKetinggian = 0, valueJarakKeahlian = 0, valueJarakBiaya = 0, valueKetinggianKeahlian = 0, valueKetinggianBiaya = 0, valueKeahlianBiaya = 0;
    RangeBar rangebar1, rangebar2, rangebar3, rangebar4, rangebar5, rangebar6;
    SliderView sliderView;
    Toolbar toolbar;
    ImageView imgAbout;

    private RadioButton rdJarakDaripadaKetinggian, rdKetinggianDaripadaJarak;
    private RadioButton rdJarakDaripadaKeahlian, rdKeahlianDaripadaJarak;
    private RadioButton rdJarakDaripadaBiaya, rdBiayaDaripadaJarak;
    private RadioButton rdKetinggianDaripadaKeahlian, rdKeahlianDaripadaKetinggian;
    private RadioButton rdKetinggianDaripadaBiaya, rdBiayaDaripadaKetinggian;
    private RadioButton rdKeahlianDaripadaBiaya, rdBiayaDaripadaKeahlian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation);

        // define the radiobutton

        rdJarakDaripadaKetinggian = findViewById(R.id.pilihJarakDaripadaKetinggian);
        rdKetinggianDaripadaJarak = findViewById(R.id.pilihKetinggianaDaripadaJarak);

        rdJarakDaripadaKeahlian = findViewById(R.id.pilihJarakDaripadaKeahlian);
        rdKeahlianDaripadaJarak = findViewById(R.id.pilihKeahlianDaripadaJarak);

        rdJarakDaripadaBiaya = findViewById(R.id.pilihJarakDaripadaBiaya);
        rdBiayaDaripadaJarak = findViewById(R.id.pilihBiayaDaripadaJarak);

        rdKetinggianDaripadaKeahlian = findViewById(R.id.pilihKetinggianDaripadaKeahlian);
        rdKeahlianDaripadaKetinggian = findViewById(R.id.pilihKeahlianDaripadaKetinggian);

        rdKetinggianDaripadaBiaya = findViewById(R.id.pilihKetinggianDaripadaBiaya);
        rdBiayaDaripadaKetinggian = findViewById(R.id.pilihBiayaDaripadaKetinggian);

        rdKeahlianDaripadaBiaya = findViewById(R.id.pilihKehalianDaripadaBiaya);
        rdBiayaDaripadaKeahlian = findViewById(R.id.pilihBiayaDaripadaKeahlian);

        sliderView = findViewById(R.id.imageSlider);
        toolbar = findViewById(R.id.toolbar);
        imgAbout = toolbar.findViewById(R.id.img_about);

        // Rangebar jarak terhadap harga
        rangebar1 = findViewById(R.id.rangebar1);

        rangebar1.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueJarakKetinggian = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        // Rangebar jarak terhadap fasilitas
        rangebar2 = findViewById(R.id.rangebar2);

        rangebar2.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueJarakKeahlian = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        // Rangebar jarak terhadap akses
        rangebar3 = findViewById(R.id.rangebar3);

        rangebar3.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueJarakBiaya = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        // Rangebar harga terhadap fasilitas
        rangebar4 = findViewById(R.id.rangebar4);

        rangebar4.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueKetinggianKeahlian = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        // Rangebar harga terhadap akses
        rangebar5 = findViewById(R.id.rangebar5);

        rangebar5.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueKetinggianBiaya = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        // Rangebar fasilitas terhadap akses
        rangebar6 = findViewById(R.id.rangebar6);

        rangebar6.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                valueKeahlianBiaya = Integer.parseInt(rightPinValue);
            }

            @Override
            public void onTouchEnded(RangeBar rangeBar) {

            }

            @Override
            public void onTouchStarted(RangeBar rangeBar) {

            }
        });

        imgAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(RecommendationActivity.this, AboutActivity.class));
            }
        });

        setupImageSlider();
    }

    public void onClickResult(View view) {
        Intent intent = new Intent(this, RecommendationResultActivity.class);
        intent.putExtra("value-jarak-ketinggian", "" + valueJarakKetinggian);
        intent.putExtra("value-jarak-keahlian", "" + valueJarakKeahlian);
        intent.putExtra("value-jarak-biaya", "" + valueJarakBiaya);
        intent.putExtra("value-ketinggian-keahlian", "" + valueKetinggianKeahlian);
        intent.putExtra("value-ketinggian-biaya", "" + valueKetinggianBiaya);
        intent.putExtra("value-keahlian-biaya", "" + valueKeahlianBiaya);

        if (rdJarakDaripadaKetinggian.isChecked()) {
            intent.putExtra("jarak-ketinggian", "0");
        } else if (rdKetinggianDaripadaJarak.isChecked()) {
            intent.putExtra("jarak-ketinggian", "1");
        }

        if (rdJarakDaripadaKeahlian.isChecked()) {
            intent.putExtra("jarak-keahlian", "0");
        } else if (rdKeahlianDaripadaJarak.isChecked()) {
            intent.putExtra("jarak-keahlian", "1");
        }

        if (rdJarakDaripadaBiaya.isChecked()) {
            intent.putExtra("jarak-biaya", "0");
        } else if (rdBiayaDaripadaJarak.isChecked()) {
            intent.putExtra("jarak-biaya", "1");
        }

        if (rdKetinggianDaripadaKeahlian.isChecked()) {
            intent.putExtra("ketinggian-keahlian", "0");
        } else if (rdKeahlianDaripadaKetinggian.isChecked()) {
            intent.putExtra("ketinggian-keahlian", "1");
        }

        if (rdKetinggianDaripadaBiaya.isChecked()) {
            intent.putExtra("ketinggian-biaya", "0");
        } else if (rdBiayaDaripadaKetinggian.isChecked()) {
            intent.putExtra("ketinggian-biaya", "1");
        }

        if (rdKeahlianDaripadaBiaya.isChecked()) {
            intent.putExtra("keahlian-biaya", "0");
        } else if (rdBiayaDaripadaKeahlian.isChecked()) {
            intent.putExtra("keahlian-biaya", "1");
        }

        startActivity(intent);
    }

    private void setupImageSlider(){
        List<ImageSliderModel> imageSliderModelList = new ArrayList<>();
        ImageSliderModel model1 = new ImageSliderModel();
        model1.setImageUrl(R.drawable.ic_andong);
        imageSliderModelList.add(model1);
        ImageSliderModel model2 = new ImageSliderModel();
        model2.setImageUrl(R.drawable.ic_kembang);
        imageSliderModelList.add(model2);
        ImageSliderModel model3 = new ImageSliderModel();
        model3.setImageUrl(R.drawable.ic_langeran);
        imageSliderModelList.add(model3);
        ImageSliderModel model4 = new ImageSliderModel();
        model4.setImageUrl(R.drawable.ic_lawu);
        imageSliderModelList.add(model4);
        ImageSliderModel model5 = new ImageSliderModel();
        model5.setImageUrl(R.drawable.ic_merapi);
        imageSliderModelList.add(model5);
        ImageSliderModel model6 = new ImageSliderModel();
        model6.setImageUrl(R.drawable.ic_merbabu);
        imageSliderModelList.add(model6);
        ImageSliderModel model7 = new ImageSliderModel();
        model7.setImageUrl(R.drawable.ic_muria);
        imageSliderModelList.add(model7);
        ImageSliderModel model8 = new ImageSliderModel();
        model8.setImageUrl(R.drawable.ic_prau);
        imageSliderModelList.add(model8);
        ImageSliderModel model9 = new ImageSliderModel();
        model9.setImageUrl(R.drawable.ic_sigandul);
        imageSliderModelList.add(model9);
        ImageSliderModel model10 = new ImageSliderModel();
        model10.setImageUrl(R.drawable.ic_sikendil);
        imageSliderModelList.add(model10);
        ImageSliderModel model11 = new ImageSliderModel();
        model11.setImageUrl(R.drawable.ic_sindoro);
        imageSliderModelList.add(model11);
        ImageSliderModel model12 = new ImageSliderModel();
        model12.setImageUrl(R.drawable.ic_slamet);
        imageSliderModelList.add(model12);
        ImageSliderModel model13 = new ImageSliderModel();
        model13.setImageUrl(R.drawable.ic_sumbing);
        imageSliderModelList.add(model13);
        ImageSliderModel model14 = new ImageSliderModel();
        model14.setImageUrl(R.drawable.ic_telomoyo);
        imageSliderModelList.add(model14);
        ImageSliderModel model15 = new ImageSliderModel();
        model15.setImageUrl(R.drawable.ic_ungaran);
        imageSliderModelList.add(model15);

        ImageSliderAdapter adapter = new ImageSliderAdapter(this, imageSliderModelList);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(2);
        sliderView.startAutoCycle();

    }
}