package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class PerlengkapanActivity extends AppCompatActivity {

    DatabaseHandler dbHandler;
    ImageView imgGunung;
    TextView tvPeralatan;
    Button btnMengerti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perlengkapan);

        imgGunung = findViewById(R.id.img_gunung);
        tvPeralatan = findViewById(R.id.tv_peralatan);
        btnMengerti = findViewById(R.id.btn_mengerti);

        dbHandler = new DatabaseHandler(PerlengkapanActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        Glide.with(PerlengkapanActivity.this).load(gunungModel.getFoto()).into(imgGunung);

        if (!gunungModel.getPeralatan().equals("")){
            tvPeralatan.setText(gunungModel.getPeralatan());
        } else {
            tvPeralatan.setText("Peralatan tidak tersedia");
        }

        btnMengerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}