package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class PendakianActivity extends AppCompatActivity {

    DatabaseHandler dbHandler;
    ImageView imgGunung, imgJalur;
    Button btnMengerti;
    TextView tvEstimasi, tvDurasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendakian);

        imgGunung = findViewById(R.id.img_gunung);
        imgJalur = findViewById(R.id.img_jalur);
        btnMengerti = findViewById(R.id.btn_mengerti);
        tvEstimasi = findViewById(R.id.tv_estimasi_pendakian);
        tvDurasi = findViewById(R.id.tv_durasi_pendakian);

        dbHandler = new DatabaseHandler(PendakianActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        if (gunungModel.getJalur_pendakian_image() != 0){
            imgJalur.setVisibility(View.VISIBLE);
            Glide.with(PendakianActivity.this).load(gunungModel.getJalur_pendakian_image()).into(imgJalur);
        } else {
            imgJalur.setVisibility(View.GONE);
        }

        tvEstimasi.setText(gunungModel.getEstimasi());
        tvDurasi.setText(gunungModel.getDurasi());

        btnMengerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}