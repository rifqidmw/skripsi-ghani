package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class DetailActivity extends AppCompatActivity {

    ImageView imgGunung;
    Button btnSop, btnPeralatan, btnBiaya, btnPendakian;
    DatabaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imgGunung = findViewById(R.id.img_gunung);
        btnSop = findViewById(R.id.btn_sop);
        btnPeralatan = findViewById(R.id.btn_peralatan);
        btnBiaya = findViewById(R.id.btn_estimasi);
        btnPendakian = findViewById(R.id.btn_pendakian);

        dbHandler = new DatabaseHandler(DetailActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        Glide.with(DetailActivity.this).load(gunungModel.getFoto()).into(imgGunung);

        btnSop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, SopActivity.class);
                intent.putExtra("ID_GUNUNG", getIntent.getIntExtra("ID_GUNUNG", 0));
                startActivity(intent);
            }
        });

        btnPeralatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, PerlengkapanActivity.class);
                intent.putExtra("ID_GUNUNG", getIntent.getIntExtra("ID_GUNUNG", 0));
                startActivity(intent);
            }
        });

        btnBiaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, EstimasiActivity.class);
                intent.putExtra("ID_GUNUNG", getIntent.getIntExtra("ID_GUNUNG", 0));
                startActivity(intent);
            }
        });

        btnPendakian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailActivity.this, PendakianActivity.class);
                intent.putExtra("ID_GUNUNG", getIntent.getIntExtra("ID_GUNUNG", 0));
                startActivity(intent);
            }
        });
    }
}