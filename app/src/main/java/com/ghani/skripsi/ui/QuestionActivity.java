package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.chivorn.smartmaterialspinner.SmartMaterialSpinner;
import com.ghani.skripsi.R;
import com.ghani.skripsi.adapter.ImageSliderAdapter;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.ImageSliderModel;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class QuestionActivity extends AppCompatActivity {
    Button btnCari;
    DatabaseHandler dbHandler;
    SliderView sliderView;
    Toolbar toolbar;
    ImageView imgAbout;
    SmartMaterialSpinner spinner1, spinner2, spinner3, spinner4;

    int flag=0;
    int value=0;
    int valueJarak=0;
    int valueKetinggian=0;
    int valueKeahlian=0;
    int valueBiaya=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        dbHandler = new DatabaseHandler(QuestionActivity.this);
        sliderView = findViewById(R.id.imageSlider);

        spinner1 = findViewById(R.id.sp_jarak);
        spinner2 = findViewById(R.id.sp_ketinggian);
        spinner3 = findViewById(R.id.sp_keahlian);
        spinner4 = findViewById(R.id.sp_biaya);
        toolbar = findViewById(R.id.toolbar);
        imgAbout = toolbar.findViewById(R.id.img_about);
        btnCari = findViewById(R.id.btn_cari);

        setupSpinner();
        setupImageSlider();

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (valueJarak != 0 && valueKetinggian != 0  && valueBiaya != 0){
                    Intent intent = new Intent(QuestionActivity.this, ResultActivity.class);
                    intent.putExtra("RESULT_VALUE", valueJarak+valueKetinggian+valueKeahlian+valueBiaya);
                    startActivity(intent);
                } else {
                    Toast.makeText(QuestionActivity.this, "Harap dipilih semua kriteria!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        imgAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(QuestionActivity.this, AboutActivity.class));
            }
        });
    }

    private void setupImageSlider(){
        List<ImageSliderModel> imageSliderModelList = new ArrayList<>();
        ImageSliderModel model1 = new ImageSliderModel();
        model1.setImageUrl(R.drawable.ic_andong);
        imageSliderModelList.add(model1);
        ImageSliderModel model2 = new ImageSliderModel();
        model2.setImageUrl(R.drawable.ic_kembang);
        imageSliderModelList.add(model2);
        ImageSliderModel model3 = new ImageSliderModel();
        model3.setImageUrl(R.drawable.ic_langeran);
        imageSliderModelList.add(model3);
        ImageSliderModel model4 = new ImageSliderModel();
        model4.setImageUrl(R.drawable.ic_lawu);
        imageSliderModelList.add(model4);
        ImageSliderModel model5 = new ImageSliderModel();
        model5.setImageUrl(R.drawable.ic_merapi);
        imageSliderModelList.add(model5);
        ImageSliderModel model6 = new ImageSliderModel();
        model6.setImageUrl(R.drawable.ic_merbabu);
        imageSliderModelList.add(model6);
        ImageSliderModel model7 = new ImageSliderModel();
        model7.setImageUrl(R.drawable.ic_muria);
        imageSliderModelList.add(model7);
        ImageSliderModel model8 = new ImageSliderModel();
        model8.setImageUrl(R.drawable.ic_prau);
        imageSliderModelList.add(model8);
        ImageSliderModel model9 = new ImageSliderModel();
        model9.setImageUrl(R.drawable.ic_sigandul);
        imageSliderModelList.add(model9);
        ImageSliderModel model10 = new ImageSliderModel();
        model10.setImageUrl(R.drawable.ic_sikendil);
        imageSliderModelList.add(model10);
        ImageSliderModel model11 = new ImageSliderModel();
        model11.setImageUrl(R.drawable.ic_sindoro);
        imageSliderModelList.add(model11);
        ImageSliderModel model12 = new ImageSliderModel();
        model12.setImageUrl(R.drawable.ic_slamet);
        imageSliderModelList.add(model12);
        ImageSliderModel model13 = new ImageSliderModel();
        model13.setImageUrl(R.drawable.ic_sumbing);
        imageSliderModelList.add(model13);
        ImageSliderModel model14 = new ImageSliderModel();
        model14.setImageUrl(R.drawable.ic_telomoyo);
        imageSliderModelList.add(model14);
        ImageSliderModel model15 = new ImageSliderModel();
        model15.setImageUrl(R.drawable.ic_ungaran);
        imageSliderModelList.add(model15);

        ImageSliderAdapter adapter = new ImageSliderAdapter(this, imageSliderModelList);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM);
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(2);
        sliderView.startAutoCycle();

    }

    private void setupSpinner(){
        ArrayList<String> dataJarak = new ArrayList<>();
        dataJarak.add("1 KM");
        dataJarak.add("2 KM");
        dataJarak.add("3 KM");
        dataJarak.add("4 KM");
        spinner1.setItem(dataJarak);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                valueJarak = i+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayList<String> dataKetinggian = new ArrayList<>();
        dataKetinggian.add("1000 MDPL");
        dataKetinggian.add("2000 MDPL");
        dataKetinggian.add("3000 MDPL");
        dataKetinggian.add("4000 MDPL");

        spinner2.setItem(dataKetinggian);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                valueKetinggian = i+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*ArrayList<String> dataKeahlian = new ArrayList<>();
        dataKeahlian.add("Belum Pernah");
        dataKeahlian.add("Sudah Pernah");
        spinner3.setItem(dataKeahlian);
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                valueKeahlian = i+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        ArrayList<String> dataBiaya = new ArrayList<>();
        dataBiaya.add("Rp. 100 Ribu");
        dataBiaya.add("Rp. 200 Ribu");
        dataBiaya.add("Rp. 300 Ribu");
        dataBiaya.add("Rp. 500 Ribu");

        spinner4.setItem(dataBiaya);
        spinner4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                valueBiaya = i+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}