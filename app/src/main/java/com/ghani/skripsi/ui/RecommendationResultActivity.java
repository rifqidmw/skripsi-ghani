package com.ghani.skripsi.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.santalu.diagonalimageview.DiagonalImageView;

import java.util.ArrayList;

public class RecommendationResultActivity extends AppCompatActivity {

    private final String TAG = RecommendationActivity.class.getSimpleName();

    double valueJarakToKetinggian, valueJarakToKeahlian, valueJarakToBiaya, valueKetinggianToKeahlian, valueKetinggianToBiaya, valueKeahliahToBiaya;
    double valueKetinggianToJarak, valueKeahlianToJarak, valueBiayaToJarak, valueKeahlianToKetinggian, valueBiayaToKetinggian, valueBiayaToKeahlian;

    double[][] matrix = new double[4][4];
    double[][] matrixC = new double[4][4];

    double kolomA, kolomB, kolomC, kolomD;
    double bobotPrioritasA, bobotPrioritasB, bobotPrioritasC, bobotPrioritasD;

    //1
    double nilaiAlternatifJarakAndong, nilaiAlternatifJarakKembang, nilaiAlternatifJarakLangeran, nilaiAlternatifJarakLawu, nilaiAlternatifJarakMerapi, nilaiAlternatifJarakMerbabu, nilaiAlternatifJarakMuria, nilaiAlternatifJarakPrau, nilaiAlternatifJarakSigandul, nilaiAlternatifJarakSikendil, nilaiAlternatifJarakSindoro, nilaiAlternatifJarakSlamet, nilaiAlternatifJarakSumbing, nilaiAlternatifJarakTelomoyo, nilaiAlternatifJarakUngaran;
    double nilaiAlternatifBiayaAndong, nilaiAlternatifBiayaKembang, nilaiAlternatifBiayaLangeran, nilaiAlternatifBiayaLawu, nilaiAlternatifBiayaMerapi, nilaiAlternatifBiayaMerbabu, nilaiAlternatifBiayaMuria, nilaiAlternatifBiayaPrau, nilaiAlternatifBiayaSigandul, nilaiAlternatifBiayaSikendil, nilaiAlternatifBiayaSindoro, nilaiAlternatifBiayaSlamet, nilaiAlternatifBiayaSumbing, nilaiAlternatifBiayaTelomoyo, nilaiAlternatifBiayaUngaran;
    double nilaiAlternatifKeahlianAndong, nilaiAlternatifKeahlianKembang, nilaiAlternatifKeahlianLangeran, nilaiAlternatifKeahlianLawu, nilaiAlternatifKeahlianMerapi, nilaiAlternatifKeahlianMuria, nilaiAlternatifKeahlianPrau, nilaiAlternatifKeahlianSigandul, nilaiAlternatifKeahlianSikendil, nilaiAlternatifKeahlianSindoro, nilaiAlternatifKeahlianSlamet, nilaiAlternatifKeahlianSumbing, nilaiAlternatifKeahlianTelomoyo, nilaiAlternatifKeahlianUngaran;
    double nilaiAlternatifKetinggianAndong, nilaiAlternatifKetinggianKembang, nilaiAlternatifKetinggianLangeran, nilaiAlternatifKetinggianLawu, nilaiAlternatifKetinggianMerapi, nilaiAlternatifKetinggianMerbabu, nilaiAlternatifKetinggianMuria, nilaiAlternatifKetinggianPrau, nilaiAlternatifKetinggianSigandul, nilaiAlternatifKetinggianSikendil, nilaiAlternatifKetinggianSindoro, nilaiAlternatifKetinggianSlamet, nilaiAlternatifKetinggianSumbing, nilaiAlternatifKetinggianTelomoyo, nilaiAlternatifKetinggianUngaran;

    double bobotPrioritasJarakAndong, bobotPrioritasBiayaAndong, bobotPrioritasKeahlianAndong, bobotPrioritasKetinggianAndong;
    double bobotPrioritasJarakKembang, bobotPrioritasBiayaKembang, bobotPrioritasKeahlianKembang, bobotPrioritasKetinggianKembang;
    double bobotPrioritasJarakLangeran, bobotPrioritasBiayaLangeran, bobotPrioritasKeahlianLangeran, bobotPrioritasKetinggianLangeran;
    double bobotPrioritasJarakLawu, bobotPrioritasBiayaLawu, bobotPrioritasKeahlianLawu, bobotPrioritasKetinggianLawu;
    double bobotPrioritasJarakMerapi, bobotPrioritasBiayaMerapi, bobotPrioritasKeahlianMerapi, bobotPrioritasKetinggianMerapi;
    double bobotPrioritasJarakMerbabu, bobotPrioritasBiayaMerbabu, bobotPrioritasKeahlianMerbabu, bobotPrioritasKetinggianMerbabu;
    double bobotPrioritasJarakMuria, bobotPrioritasBiayaMuria, bobotPrioritasKeahlianMuria, bobotPrioritasKetinggianMuria;
    double bobotPrioritasJarakPrau, bobotPrioritasBiayaPrau, bobotPrioritasKeahlianPrau, bobotPrioritasKetinggianPrau;
    double bobotPrioritasJarakSigandul, bobotPrioritasBiayaSigandul, bobotPrioritasKeahlianSigandul, bobotPrioritasKetinggianSigandul;
    double bobotPrioritasJarakSikendil, bobotPrioritasBiayaSikendil, bobotPrioritasKeahlianSikendil, bobotPrioritasKetinggianSikendil;
    double bobotPrioritasJarakSindoro, bobotPrioritasBiayaSindoro, bobotPrioritasKeahlianSindoro, bobotPrioritasKetinggianSindoro;
    double bobotPrioritasJarakSlamet, bobotPrioritasBiayaSlamet, bobotPrioritasKeahlianSlamet, bobotPrioritasKetinggianSlamet;
    double bobotPrioritasJarakSumbing, bobotPrioritasBiayaSumbing, bobotPrioritasKeahlianSumbing, bobotPrioritasKetinggianSumbing;
    double bobotPrioritasJarakTelomoyo, bobotPrioritasBiayaTelomoyo, bobotPrioritasKeahlianTelomoyo, bobotPrioritasKetinggianTelomoyo;
    double bobotPrioritasJarakUngaran, bobotPrioritasBiayaUngaran, bobotPrioritasKeahlianUngaran, bobotPrioritasKetinggianUngaran;

    double nilaiTotalAndong = 0, nilaiTotalKembang = 0, nilaiTotalLangeran = 0, nilaiTotalLawu = 0, nilaiTotalMerapi = 0, nilaiTotalMerbabu = 0, nilaiTotalMuria = 0, nilaiTotalPrau = 0, nilaiTotalSigandul = 0, nilaiTotalSikendil = 0, nilaiTotalSindoro = 0, nilaiTotalSlamet = 0, nilaiTotalSumbing = 0, nilaiTotalTelomoyo = 0, nilaiTotalUngaran = 0;

    //2
    double nilaiAlternatif2JarakAndong, nilaiAlternatif2JarakKembang, nilaiAlternatif2JarakLangeran, nilaiAlternatif2JarakLawu, nilaiAlternatif2JarakMerapi, nilaiAlternatif2JarakMerbabu, nilaiAlternatif2JarakMuria, nilaiAlternatif2JarakPrau, nilaiAlternatif2JarakSigandul, nilaiAlternatif2JarakSikendil, nilaiAlternatif2JarakSindoro, nilaiAlternatif2JarakSlamet, nilaiAlternatif2JarakSumbing, nilaiAlternatif2JarakTelomoyo, nilaiAlternatif2JarakUngaran;
    double nilaiAlternatif2BiayaAndong, nilaiAlternatif2BiayaKembang, nilaiAlternatif2BiayaLangeran, nilaiAlternatif2BiayaLawu, nilaiAlternatif2BiayaMerapi, nilaiAlternatif2BiayaMerbabu, nilaiAlternatif2BiayaMuria, nilaiAlternatif2BiayaPrau, nilaiAlternatif2BiayaSigandul, nilaiAlternatif2BiayaSikendil, nilaiAlternatif2BiayaSindoro, nilaiAlternatif2BiayaSlamet, nilaiAlternatif2BiayaSumbing, nilaiAlternatif2BiayaTelomoyo, nilaiAlternatif2BiayaUngaran;
    double nilaiAlternatif2KeahlianAndong, nilaiAlternatif2KeahlianKembang, nilaiAlternatif2KeahlianLangeran, nilaiAlternatif2KeahlianLawu, nilaiAlternatif2KeahlianMerapi, nilaiAlternatif2KeahlianMuria, nilaiAlternatif2KeahlianPrau, nilaiAlternatif2KeahlianSigandul, nilaiAlternatif2KeahlianSikendil, nilaiAlternatif2KeahlianSindoro, nilaiAlternatif2KeahlianSlamet, nilaiAlternatif2KeahlianSumbing, nilaiAlternatif2KeahlianTelomoyo, nilaiAlternatif2KeahlianUngaran;
    double nilaiAlternatif2KetinggianAndong, nilaiAlternatif2KetinggianKembang, nilaiAlternatif2KetinggianLangeran, nilaiAlternatif2KetinggianLawu, nilaiAlternatif2KetinggianMerapi, nilaiAlternatif2KetinggianMerbabu, nilaiAlternatif2KetinggianMuria, nilaiAlternatif2KetinggianPrau, nilaiAlternatif2KetinggianSigandul, nilaiAlternatif2KetinggianSikendil, nilaiAlternatif2KetinggianSindoro, nilaiAlternatif2KetinggianSlamet, nilaiAlternatif2KetinggianSumbing, nilaiAlternatif2KetinggianTelomoyo, nilaiAlternatif2KetinggianUngaran;

    double bobotPrioritas2JarakAndong, bobotPrioritas2BiayaAndong, bobotPrioritas2KeahlianAndong, bobotPrioritas2KetinggianAndong;
    double bobotPrioritas2JarakKembang, bobotPrioritas2BiayaKembang, bobotPrioritas2KeahlianKembang, bobotPrioritas2KetinggianKembang;
    double bobotPrioritas2JarakLangeran, bobotPrioritas2BiayaLangeran, bobotPrioritas2KeahlianLangeran, bobotPrioritas2KetinggianLangeran;
    double bobotPrioritas2JarakLawu, bobotPrioritas2BiayaLawu, bobotPrioritas2KeahlianLawu, bobotPrioritas2KetinggianLawu;
    double bobotPrioritas2JarakMerapi, bobotPrioritas2BiayaMerapi, bobotPrioritas2KeahlianMerapi, bobotPrioritas2KetinggianMerapi;
    double bobotPrioritas2JarakMerbabu, bobotPrioritas2BiayaMerbabu, bobotPrioritas2KeahlianMerbabu, bobotPrioritas2KetinggianMerbabu;
    double bobotPrioritas2JarakMuria, bobotPrioritas2BiayaMuria, bobotPrioritas2KeahlianMuria, bobotPrioritas2KetinggianMuria;
    double bobotPrioritas2JarakPrau, bobotPrioritas2BiayaPrau, bobotPrioritas2KeahlianPrau, bobotPrioritas2KetinggianPrau;
    double bobotPrioritas2JarakSigandul, bobotPrioritas2BiayaSigandul, bobotPrioritas2KeahlianSigandul, bobotPrioritas2KetinggianSigandul;
    double bobotPrioritas2JarakSikendil, bobotPrioritas2BiayaSikendil, bobotPrioritas2KeahlianSikendil, bobotPrioritas2KetinggianSikendil;
    double bobotPrioritas2JarakSindoro, bobotPrioritas2BiayaSindoro, bobotPrioritas2KeahlianSindoro, bobotPrioritas2KetinggianSindoro;
    double bobotPrioritas2JarakSlamet, bobotPrioritas2BiayaSlamet, bobotPrioritas2KeahlianSlamet, bobotPrioritas2KetinggianSlamet;
    double bobotPrioritas2JarakSumbing, bobotPrioritas2BiayaSumbing, bobotPrioritas2KeahlianSumbing, bobotPrioritas2KetinggianSumbing;
    double bobotPrioritas2JarakTelomoyo, bobotPrioritas2BiayaTelomoyo, bobotPrioritas2KeahlianTelomoyo, bobotPrioritas2KetinggianTelomoyo;
    double bobotPrioritas2JarakUngaran, bobotPrioritas2BiayaUngaran, bobotPrioritas2KeahlianUngaran, bobotPrioritas2KetinggianUngaran;

    double nilaiTotal2Andong = 0, nilaiTotal2Kembang = 0, nilaiTotal2Langeran = 0, nilaiTotal2Lawu = 0, nilaiTotal2Merapi = 0, nilaiTotal2Merbabu = 0, nilaiTotal2Muria = 0, nilaiTotal2Prau = 0, nilaiTotal2Sigandul = 0, nilaiTotal2Sikendil = 0, nilaiTotal2Sindoro = 0, nilaiTotal2Slamet = 0, nilaiTotal2Sumbing = 0, nilaiTotal2Telomoyo = 0, nilaiTotal2Ungaran = 0;

    //3
    double nilaiAlternatif3JarakAndong, nilaiAlternatif3JarakKembang, nilaiAlternatif3JarakLangeran, nilaiAlternatif3JarakLawu, nilaiAlternatif3JarakMerapi, nilaiAlternatif3JarakMerbabu, nilaiAlternatif3JarakMuria, nilaiAlternatif3JarakPrau, nilaiAlternatif3JarakSigandul, nilaiAlternatif3JarakSikendil, nilaiAlternatif3JarakSindoro, nilaiAlternatif3JarakSlamet, nilaiAlternatif3JarakSumbing, nilaiAlternatif3JarakTelomoyo, nilaiAlternatif3JarakUngaran;
    double nilaiAlternatif3BiayaAndong, nilaiAlternatif3BiayaKembang, nilaiAlternatif3BiayaLangeran, nilaiAlternatif3BiayaLawu, nilaiAlternatif3BiayaMerapi, nilaiAlternatif3BiayaMerbabu, nilaiAlternatif3BiayaMuria, nilaiAlternatif3BiayaPrau, nilaiAlternatif3BiayaSigandul, nilaiAlternatif3BiayaSikendil, nilaiAlternatif3BiayaSindoro, nilaiAlternatif3BiayaSlamet, nilaiAlternatif3BiayaSumbing, nilaiAlternatif3BiayaTelomoyo, nilaiAlternatif3BiayaUngaran;
    double nilaiAlternatif3KeahlianAndong, nilaiAlternatif3KeahlianKembang, nilaiAlternatif3KeahlianLangeran, nilaiAlternatif3KeahlianLawu, nilaiAlternatif3KeahlianMerapi, nilaiAlternatif3KeahlianMuria, nilaiAlternatif3KeahlianPrau, nilaiAlternatif3KeahlianSigandul, nilaiAlternatif3KeahlianSikendil, nilaiAlternatif3KeahlianSindoro, nilaiAlternatif3KeahlianSlamet, nilaiAlternatif3KeahlianSumbing, nilaiAlternatif3KeahlianTelomoyo, nilaiAlternatif3KeahlianUngaran;
    double nilaiAlternatif3KetinggianAndong, nilaiAlternatif3KetinggianKembang, nilaiAlternatif3KetinggianLangeran, nilaiAlternatif3KetinggianLawu, nilaiAlternatif3KetinggianMerapi, nilaiAlternatif3KetinggianMerbabu, nilaiAlternatif3KetinggianMuria, nilaiAlternatif3KetinggianPrau, nilaiAlternatif3KetinggianSigandul, nilaiAlternatif3KetinggianSikendil, nilaiAlternatif3KetinggianSindoro, nilaiAlternatif3KetinggianSlamet, nilaiAlternatif3KetinggianSumbing, nilaiAlternatif3KetinggianTelomoyo, nilaiAlternatif3KetinggianUngaran;

    double bobotPrioritas3JarakAndong, bobotPrioritas3BiayaAndong, bobotPrioritas3KeahlianAndong, bobotPrioritas3KetinggianAndong;
    double bobotPrioritas3JarakKembang, bobotPrioritas3BiayaKembang, bobotPrioritas3KeahlianKembang, bobotPrioritas3KetinggianKembang;
    double bobotPrioritas3JarakLangeran, bobotPrioritas3BiayaLangeran, bobotPrioritas3KeahlianLangeran, bobotPrioritas3KetinggianLangeran;
    double bobotPrioritas3JarakLawu, bobotPrioritas3BiayaLawu, bobotPrioritas3KeahlianLawu, bobotPrioritas3KetinggianLawu;
    double bobotPrioritas3JarakMerapi, bobotPrioritas3BiayaMerapi, bobotPrioritas3KeahlianMerapi, bobotPrioritas3KetinggianMerapi;
    double bobotPrioritas3JarakMerbabu, bobotPrioritas3BiayaMerbabu, bobotPrioritas3KeahlianMerbabu, bobotPrioritas3KetinggianMerbabu;
    double bobotPrioritas3JarakMuria, bobotPrioritas3BiayaMuria, bobotPrioritas3KeahlianMuria, bobotPrioritas3KetinggianMuria;
    double bobotPrioritas3JarakPrau, bobotPrioritas3BiayaPrau, bobotPrioritas3KeahlianPrau, bobotPrioritas3KetinggianPrau;
    double bobotPrioritas3JarakSigandul, bobotPrioritas3BiayaSigandul, bobotPrioritas3KeahlianSigandul, bobotPrioritas3KetinggianSigandul;
    double bobotPrioritas3JarakSikendil, bobotPrioritas3BiayaSikendil, bobotPrioritas3KeahlianSikendil, bobotPrioritas3KetinggianSikendil;
    double bobotPrioritas3JarakSindoro, bobotPrioritas3BiayaSindoro, bobotPrioritas3KeahlianSindoro, bobotPrioritas3KetinggianSindoro;
    double bobotPrioritas3JarakSlamet, bobotPrioritas3BiayaSlamet, bobotPrioritas3KeahlianSlamet, bobotPrioritas3KetinggianSlamet;
    double bobotPrioritas3JarakSumbing, bobotPrioritas3BiayaSumbing, bobotPrioritas3KeahlianSumbing, bobotPrioritas3KetinggianSumbing;
    double bobotPrioritas3JarakTelomoyo, bobotPrioritas3BiayaTelomoyo, bobotPrioritas3KeahlianTelomoyo, bobotPrioritas3KetinggianTelomoyo;
    double bobotPrioritas3JarakUngaran, bobotPrioritas3BiayaUngaran, bobotPrioritas3KeahlianUngaran, bobotPrioritas3KetinggianUngaran;

    double nilaiTotal3Andong = 0, nilaiTotal3Kembang = 0, nilaiTotal3Langeran = 0, nilaiTotal3Lawu = 0, nilaiTotal3Merapi = 0, nilaiTotal3Merbabu = 0, nilaiTotal3Muria = 0, nilaiTotal3Prau = 0, nilaiTotal3Sigandul = 0, nilaiTotal3Sikendil = 0, nilaiTotal3Sindoro = 0, nilaiTotal3Slamet = 0, nilaiTotal3Sumbing = 0, nilaiTotal3Telomoyo = 0, nilaiTotal3Ungaran = 0;
    double nilaiMainAndong = 0, nilaiMainKembang = 0, nilaiMainLangeran = 0, nilaiMainLawu = 0, nilaiMainMerapi = 0, nilaiMainMerbabu = 0, nilaiMainMuria = 0, nilaiMainPrau = 0, nilaiMainSigandul = 0, nilaiMainSikendil = 0, nilaiMainSindoro = 0, nilaiMainSlamet = 0, nilaiMainSumbing = 0, nilaiMainTelomoyo = 0, nilaiMainUngaran = 0;

    private DatabaseReference mDatabase;
    private DatabaseHandler dbHandler;
    TextView tvResult1, tvResult2;
    DiagonalImageView imgResult1, imgResult2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation_result);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        dbHandler = new DatabaseHandler(RecommendationResultActivity.this);
        tvResult1 = findViewById(R.id.tv_result_1);
        tvResult2 = findViewById(R.id.tv_result_2);
        imgResult1 = findViewById(R.id.img_1);
        imgResult2 = findViewById(R.id.img_2);

        valueKetinggianToJarak = 1 / Double.valueOf("" + getIntent().getStringExtra("value-jarak-ketinggian"));

        // Rangebar Jarak Fasilitas
        valueKeahlianToJarak = 1 / Double.valueOf("" + getIntent().getStringExtra("value-jarak-keahlian"));

        // Rangebar Jarak Akses
        valueBiayaToJarak = 1 / Double.valueOf("" + getIntent().getStringExtra("value-jarak-biaya"));

        // Rangebar Harga Fasilitas
        valueKeahlianToKetinggian = 1 / Double.valueOf("" + getIntent().getStringExtra("value-ketinggian-keahlian"));

        // Rangebar Harga Akses
        valueBiayaToKetinggian = 1 / Double.valueOf("" + getIntent().getStringExtra("value-ketinggian-biaya"));

        // Rangebar Fasilitas Akses
        valueBiayaToKeahlian = 1 / Double.valueOf("" + getIntent().getStringExtra("value-keahlian-biaya"));

        // status slider

        String statusJarakKetinggian = getIntent().getStringExtra("jarak-ketinggian");
        String statusJarakKeahlian = getIntent().getStringExtra("jarak-keahlian");
        String statusJarakBiaya = getIntent().getStringExtra("jarak-biaya");
        String statusKetinggianKeahlian = getIntent().getStringExtra("ketinggian-keahlian");
        String statusKetinggianBiaya = getIntent().getStringExtra("ketinggian-biaya");
        String statusKeahlianBiaya = getIntent().getStringExtra("keahlian-biaya");

        // Perhitungan AHP Untuk Kriteria

        matrix[0][0] = 1;
        matrix[1][1] = 1;
        matrix[2][2] = 1;
        matrix[3][3] = 1;

        // Slider 1

        if (statusJarakKetinggian.equals("0")) {
            matrix[0][1] = Double.parseDouble(getIntent().getStringExtra("value-jarak-ketinggian"));
            matrix[1][0] = valueKetinggianToJarak;
        } else if (statusJarakKetinggian.equals("1")) {
            matrix[0][1] = valueKetinggianToJarak;
            matrix[1][0] = Double.parseDouble(getIntent().getStringExtra("value-jarak-ketinggian"));
        }

        // Slider 2

        if (statusJarakKeahlian.equals("0")) {
            matrix[0][2] = Double.parseDouble(getIntent().getStringExtra("value-jarak-keahlian"));
            matrix[2][0] = valueKeahlianToJarak;
        } else if (statusJarakKeahlian.equals("1")) {
            matrix[0][2] = valueKeahlianToJarak;
            matrix[2][0] = Double.parseDouble(getIntent().getStringExtra("value-jarak-keahlian"));
        }

        // Slider 3

        if (statusJarakBiaya.equals("0")) {
            matrix[0][3] = Double.parseDouble(getIntent().getStringExtra("value-jarak-biaya"));
            matrix[3][0] = valueBiayaToJarak;
        } else if (statusJarakBiaya.equals("1")) {
            matrix[0][3] = valueBiayaToJarak;
            matrix[3][0] = Double.parseDouble(getIntent().getStringExtra("value-jarak-biaya"));
        }

        // Slider 4

        if (statusKetinggianKeahlian.equals("0")) {
            matrix[1][2] = Double.parseDouble(getIntent().getStringExtra("value-ketinggian-keahlian"));
            matrix[2][1] = valueKeahlianToKetinggian;
        } else if (statusKetinggianKeahlian.equals("1")) {
            matrix[1][2] = valueKeahlianToKetinggian;
            matrix[2][1] = Double.parseDouble(getIntent().getStringExtra("value-ketinggian-keahlian"));
        }

        // Slider 5

        if (statusKetinggianBiaya.equals("0")) {
            matrix[1][3] = Double.parseDouble(getIntent().getStringExtra("value-ketinggian-biaya"));
            matrix[3][1] = valueBiayaToKetinggian;
        } else if (statusKetinggianBiaya.equals("1")) {
            matrix[1][2] = valueBiayaToKetinggian;
            matrix[2][1] = Double.parseDouble(getIntent().getStringExtra("value-ketinggian-biaya"));
        }

        // Slider 6

        if (statusKeahlianBiaya.equals("0")) {
            matrix[2][3] = Double.parseDouble(getIntent().getStringExtra("value-keahlian-biaya"));
            matrix[3][2] = valueBiayaToKeahlian;
        } else if (statusKeahlianBiaya.equals("1")) {
            matrix[2][3] = valueBiayaToKeahlian;
            matrix[3][2] = Double.parseDouble(getIntent().getStringExtra("value-keahlian-biaya"));
        }

        // Step 1

        for (int a = 0; a < 4; a++) {
            for (int b = 0; b < 4; b++) {
                System.out.print("[" + matrix[a][b] + "]");
            }
        }

        // Step 2 ( Dikuadratkan )

        kolomA = matrix[0][0] + matrix[1][0] + matrix[2][0] + matrix[3][0];
        kolomB = matrix[0][1] + matrix[1][1] + matrix[2][1] + matrix[3][1];
        kolomC = matrix[0][2] + matrix[1][2] + matrix[2][2] + matrix[3][2];
        kolomD = matrix[0][3] + matrix[1][3] + matrix[2][3] + matrix[3][3];

        // Step 3 ( dijumlahkan tiap baris )

        matrixC[0][0] = matrix[0][0] / kolomA;
        matrixC[1][0] = matrix[1][0] / kolomA;
        matrixC[2][0] = matrix[2][0] / kolomA;
        matrixC[3][0] = matrix[3][0] / kolomA;

        matrixC[0][1] = matrix[0][1] / kolomB;
        matrixC[1][1] = matrix[1][1] / kolomB;
        matrixC[2][1] = matrix[2][1] / kolomB;
        matrixC[3][1] = matrix[3][1] / kolomB;

        matrixC[0][2] = matrix[0][2] / kolomC;
        matrixC[1][2] = matrix[1][2] / kolomC;
        matrixC[2][2] = matrix[2][2] / kolomC;
        matrixC[3][2] = matrix[3][2] / kolomC;

        matrixC[0][3] = matrix[0][3] / kolomD;
        matrixC[1][3] = matrix[1][3] / kolomD;
        matrixC[2][3] = matrix[2][3] / kolomD;
        matrixC[3][3] = matrix[3][3] / kolomD;

        // Step 4 ( dibagi dengan jumlah )

        bobotPrioritasA = (matrixC[0][0] + matrixC[0][1] + matrixC[0][2] + matrixC[0][3]) / 4;
        bobotPrioritasB = (matrixC[1][0] + matrixC[1][1] + matrixC[1][2] + matrixC[1][3]) / 4;
        bobotPrioritasC = (matrixC[2][0] + matrixC[2][1] + matrixC[2][2] + matrixC[2][3]) / 4;
        bobotPrioritasD = (matrixC[3][0] + matrixC[3][1] + matrixC[3][2] + matrixC[3][3]) / 4;

        Log.d(TAG, "Bobot prioritas Jarak: "+bobotPrioritasA);
        Log.d(TAG, "Bobot prioritas Ketinggian: "+bobotPrioritasB);
        Log.d(TAG, "Bobot prioritas Keahlian: "+bobotPrioritasC);
        Log.d(TAG, "Bobot prioritas Biaya: "+bobotPrioritasD);

        mDatabase.child("kriteria").child("jarak").setValue(bobotPrioritasA);
        mDatabase.child("kriteria").child("ketinggian").setValue(bobotPrioritasB);
        mDatabase.child("kriteria").child("keahlian").setValue(bobotPrioritasC);
        mDatabase.child("kriteria").child("biaya").setValue(bobotPrioritasD);

        mDatabase.child("alternatif").child("alternatif-jarak").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                // Andong
                if (dataSnapshot.child("alternatif-jarak-1").child("Andong").getValue() != null) {
                    nilaiAlternatifJarakAndong = dataSnapshot.child("alternatif-jarak-1").child("Andong").getValue(Double.class);
                    bobotPrioritasJarakAndong = bobotPrioritasA * nilaiAlternatifJarakAndong;

                    nilaiTotalAndong = nilaiTotalAndong + bobotPrioritasJarakAndong;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Andong").getValue() != null) {
                    nilaiAlternatif2JarakAndong = dataSnapshot.child("alternatif-jarak-2").child("Andong").getValue(Double.class);
                    bobotPrioritas2JarakAndong = bobotPrioritasA * nilaiAlternatif2JarakAndong;

                    nilaiTotal2Andong = nilaiTotal2Andong + bobotPrioritas2JarakAndong;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Andong").getValue() != null) {
                    nilaiAlternatif3JarakAndong = dataSnapshot.child("alternatif-jarak-3").child("Andong").getValue(Double.class);
                    bobotPrioritas3JarakAndong = bobotPrioritasA * nilaiAlternatif3JarakAndong;

                    nilaiTotal3Andong = nilaiTotal3Andong + bobotPrioritas3JarakAndong;
                }

                // Kembang
                if (dataSnapshot.child("alternatif-jarak-1").child("Kembang").getValue() != null) {
                    nilaiAlternatifJarakKembang = dataSnapshot.child("alternatif-jarak-1").child("Kembang").getValue(Double.class);
                    bobotPrioritasJarakKembang = bobotPrioritasA * nilaiAlternatifJarakKembang;

                    nilaiTotalKembang = nilaiTotalKembang + bobotPrioritasJarakKembang;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Kembang").getValue() != null) {
                    nilaiAlternatif2JarakKembang = dataSnapshot.child("alternatif-jarak-2").child("Kembang").getValue(Double.class);
                    bobotPrioritas2JarakKembang = bobotPrioritasA * nilaiAlternatif2JarakKembang;

                    nilaiTotal2Kembang = nilaiTotal2Kembang + bobotPrioritas2JarakKembang;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Kembang").getValue() != null) {
                    nilaiAlternatif3JarakKembang = dataSnapshot.child("alternatif-jarak-3").child("Kembang").getValue(Double.class);
                    bobotPrioritas3JarakKembang = bobotPrioritasA * nilaiAlternatif3JarakKembang;

                    nilaiTotal3Kembang = nilaiTotal3Kembang + bobotPrioritas3JarakKembang;
                }

                // Langeran
                if (dataSnapshot.child("alternatif-jarak-1").child("Langeran").getValue() != null) {
                    nilaiAlternatifJarakLangeran = dataSnapshot.child("alternatif-jarak-1").child("Langeran").getValue(Double.class);
                    bobotPrioritasJarakLangeran = bobotPrioritasA * nilaiAlternatifJarakLangeran;

                    nilaiTotalLangeran = nilaiTotalLangeran + bobotPrioritasJarakLangeran;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Langeran").getValue() != null) {
                    nilaiAlternatif2JarakLangeran = dataSnapshot.child("alternatif-jarak-2").child("Langeran").getValue(Double.class);
                    bobotPrioritas2JarakLangeran = bobotPrioritasA * nilaiAlternatif2JarakLangeran;

                    nilaiTotal2Langeran = nilaiTotal2Langeran + bobotPrioritas2JarakLangeran;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Langeran").getValue() != null) {
                    nilaiAlternatif3JarakLangeran = dataSnapshot.child("alternatif-jarak-3").child("Langeran").getValue(Double.class);
                    bobotPrioritas3JarakLangeran = bobotPrioritasA * nilaiAlternatif3JarakLangeran;

                    nilaiTotal3Langeran = nilaiTotal3Langeran + bobotPrioritas3JarakLangeran;
                }

                // Lawu
                if (dataSnapshot.child("alternatif-jarak-1").child("Lawu").getValue() != null) {
                    nilaiAlternatifJarakLawu = dataSnapshot.child("alternatif-jarak-1").child("Lawu").getValue(Double.class);
                    bobotPrioritasJarakLawu = bobotPrioritasA * nilaiAlternatifJarakLawu;

                    nilaiTotalLawu = nilaiTotalLawu + bobotPrioritasJarakLawu;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Lawu").getValue() != null) {
                    nilaiAlternatif2JarakLawu = dataSnapshot.child("alternatif-jarak-2").child("Lawu").getValue(Double.class);
                    bobotPrioritas2JarakLawu = bobotPrioritasA * nilaiAlternatif2JarakLawu;

                    nilaiTotal2Lawu = nilaiTotal2Lawu + bobotPrioritas2JarakLawu;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Lawu").getValue() != null) {
                    nilaiAlternatif3JarakLawu = dataSnapshot.child("alternatif-jarak-3").child("Lawu").getValue(Double.class);
                    bobotPrioritas3JarakLawu = bobotPrioritasA * nilaiAlternatif3JarakLawu;

                    nilaiTotal3Lawu = nilaiTotal3Lawu + bobotPrioritas3JarakLawu;
                }

                // Merapi
                if (dataSnapshot.child("alternatif-jarak-1").child("Merapi").getValue() != null) {
                    nilaiAlternatifJarakMerapi = dataSnapshot.child("alternatif-jarak-1").child("Merapi").getValue(Double.class);
                    bobotPrioritasJarakMerapi = bobotPrioritasA * nilaiAlternatifJarakMerapi;

                    nilaiTotalMerapi = nilaiTotalMerapi + bobotPrioritasJarakMerapi;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Merapi").getValue() != null) {
                    nilaiAlternatif2JarakMerapi = dataSnapshot.child("alternatif-jarak-2").child("Merapi").getValue(Double.class);
                    bobotPrioritas2JarakMerapi = bobotPrioritasA * nilaiAlternatif2JarakMerapi;

                    nilaiTotal2Merapi = nilaiTotal2Merapi + bobotPrioritas2JarakMerapi;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Merapi").getValue() != null) {
                    nilaiAlternatif3JarakMerapi = dataSnapshot.child("alternatif-jarak-3").child("Merapi").getValue(Double.class);
                    bobotPrioritas3JarakMerapi = bobotPrioritasA * nilaiAlternatif3JarakMerapi;

                    nilaiTotal3Merapi = nilaiTotal3Merapi + bobotPrioritas3JarakMerapi;
                }

                // Merbabu
                if (dataSnapshot.child("alternatif-jarak-1").child("Merbabu").getValue() != null) {
                    nilaiAlternatifJarakMerbabu = dataSnapshot.child("alternatif-jarak-1").child("Merbabu").getValue(Double.class);
                    bobotPrioritasJarakMerbabu = bobotPrioritasA * nilaiAlternatifJarakMerbabu;

                    nilaiTotalMerbabu = nilaiTotalMerbabu + bobotPrioritasJarakMerbabu;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Merbabu").getValue() != null) {
                    nilaiAlternatif2JarakMerbabu = dataSnapshot.child("alternatif-jarak-2").child("Merbabu").getValue(Double.class);
                    bobotPrioritas2JarakMerbabu = bobotPrioritasA * nilaiAlternatif2JarakMerbabu;

                    nilaiTotal2Merbabu = nilaiTotal2Merbabu + bobotPrioritas2JarakMerbabu;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Merbabu").getValue() != null) {
                    nilaiAlternatif3JarakMerbabu = dataSnapshot.child("alternatif-jarak-3").child("Merbabu").getValue(Double.class);
                    bobotPrioritas3JarakMerbabu = bobotPrioritasA * nilaiAlternatif3JarakMerbabu;

                    nilaiTotal3Merbabu = nilaiTotal3Merbabu + bobotPrioritas3JarakMerbabu;
                }

                // Muria
                if (dataSnapshot.child("alternatif-jarak-1").child("Muria").getValue() != null) {
                    nilaiAlternatifJarakMuria = dataSnapshot.child("alternatif-jarak-1").child("Muria").getValue(Double.class);
                    bobotPrioritasJarakMuria = bobotPrioritasA * nilaiAlternatifJarakMuria;

                    nilaiTotalMuria = nilaiTotalMuria + bobotPrioritasJarakMuria;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Muria").getValue() != null) {
                    nilaiAlternatif2JarakMuria = dataSnapshot.child("alternatif-jarak-2").child("Muria").getValue(Double.class);
                    bobotPrioritas2JarakMuria = bobotPrioritasA * nilaiAlternatif2JarakMuria;

                    nilaiTotal2Muria = nilaiTotal2Muria + bobotPrioritas2JarakMuria;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Muria").getValue() != null) {
                    nilaiAlternatif3JarakMuria = dataSnapshot.child("alternatif-jarak-3").child("Muria").getValue(Double.class);
                    bobotPrioritas3JarakMuria = bobotPrioritasA * nilaiAlternatif3JarakMuria;

                    nilaiTotal3Muria = nilaiTotal3Muria + bobotPrioritas3JarakMuria;
                }

                // Prau
                if (dataSnapshot.child("alternatif-jarak-1").child("Prau").getValue() != null) {
                    nilaiAlternatifJarakPrau = dataSnapshot.child("alternatif-jarak-1").child("Prau").getValue(Double.class);
                    bobotPrioritasJarakPrau = bobotPrioritasA * nilaiAlternatifJarakPrau;

                    nilaiTotalPrau = nilaiTotalPrau + bobotPrioritasJarakPrau;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Prau").getValue() != null) {
                    nilaiAlternatif2JarakPrau = dataSnapshot.child("alternatif-jarak-2").child("Prau").getValue(Double.class);
                    bobotPrioritas2JarakPrau = bobotPrioritasA * nilaiAlternatif2JarakPrau;

                    nilaiTotal2Prau = nilaiTotal2Prau + bobotPrioritas2JarakPrau;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Prau").getValue() != null) {
                    nilaiAlternatif3JarakPrau = dataSnapshot.child("alternatif-jarak-3").child("Prau").getValue(Double.class);
                    bobotPrioritas3JarakPrau = bobotPrioritasA * nilaiAlternatif3JarakPrau;

                    nilaiTotal3Prau = nilaiTotal3Prau + bobotPrioritas3JarakPrau;
                }

                // Sigandul
                if (dataSnapshot.child("alternatif-jarak-1").child("Sigandul").getValue() != null) {
                    nilaiAlternatifJarakSigandul = dataSnapshot.child("alternatif-jarak-1").child("Sigandul").getValue(Double.class);
                    bobotPrioritasJarakSigandul = bobotPrioritasA * nilaiAlternatifJarakSigandul;

                    nilaiTotalSigandul = nilaiTotalSigandul + bobotPrioritasJarakSigandul;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Sigandul").getValue() != null) {
                    nilaiAlternatif2JarakSigandul = dataSnapshot.child("alternatif-jarak-2").child("Sigandul").getValue(Double.class);
                    bobotPrioritas2JarakSigandul = bobotPrioritasA * nilaiAlternatif2JarakSigandul;

                    nilaiTotal2Sigandul = nilaiTotal2Sigandul + bobotPrioritas2JarakSigandul;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Sigandul").getValue() != null) {
                    nilaiAlternatif3JarakSigandul = dataSnapshot.child("alternatif-jarak-3").child("Sigandul").getValue(Double.class);
                    bobotPrioritas3JarakSigandul = bobotPrioritasA * nilaiAlternatif3JarakSigandul;

                    nilaiTotal3Sigandul = nilaiTotal3Sigandul + bobotPrioritas3JarakSigandul;
                }

                // Sikendil
                if (dataSnapshot.child("alternatif-jarak-1").child("Sikendil").getValue() != null) {
                    nilaiAlternatifJarakSikendil = dataSnapshot.child("alternatif-jarak-1").child("Sikendil").getValue(Double.class);
                    bobotPrioritasJarakSikendil = bobotPrioritasA * nilaiAlternatifJarakSikendil;

                    nilaiTotalSikendil = nilaiTotalSikendil + bobotPrioritasJarakSikendil;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Sikendil").getValue() != null) {
                    nilaiAlternatif2JarakSikendil = dataSnapshot.child("alternatif-jarak-2").child("Sikendil").getValue(Double.class);
                    bobotPrioritas2JarakSikendil = bobotPrioritasA * nilaiAlternatif2JarakSikendil;

                    nilaiTotal2Sikendil = nilaiTotal2Sikendil + bobotPrioritas2JarakSikendil;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Sikendil").getValue() != null) {
                    nilaiAlternatif3JarakSikendil = dataSnapshot.child("alternatif-jarak-3").child("Sikendil").getValue(Double.class);
                    bobotPrioritas3JarakSikendil = bobotPrioritasA * nilaiAlternatif3JarakSikendil;

                    nilaiTotal3Sikendil = nilaiTotal3Sikendil + bobotPrioritas3JarakSikendil;
                }

                // Sindoro
                if (dataSnapshot.child("alternatif-jarak-1").child("Sindoro").getValue() != null) {
                    nilaiAlternatifJarakSindoro = dataSnapshot.child("alternatif-jarak-1").child("Sindoro").getValue(Double.class);
                    bobotPrioritasJarakSindoro = bobotPrioritasA * nilaiAlternatifJarakSindoro;

                    nilaiTotalSindoro = nilaiTotalSindoro + bobotPrioritasJarakSindoro;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Sindoro").getValue() != null) {
                    nilaiAlternatif2JarakSindoro = dataSnapshot.child("alternatif-jarak-2").child("Sindoro").getValue(Double.class);
                    bobotPrioritas2JarakSindoro = bobotPrioritasA * nilaiAlternatif2JarakSindoro;

                    nilaiTotal2Sindoro = nilaiTotal2Sindoro + bobotPrioritas2JarakSindoro;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Sindoro").getValue() != null) {
                    nilaiAlternatif3JarakSindoro = dataSnapshot.child("alternatif-jarak-3").child("Sindoro").getValue(Double.class);
                    bobotPrioritas3JarakSindoro = bobotPrioritasA * nilaiAlternatif3JarakSindoro;

                    nilaiTotal3Sindoro = nilaiTotal3Sindoro + bobotPrioritas3JarakSindoro;
                }

                // Slamet
                if (dataSnapshot.child("alternatif-jarak-1").child("Slamet").getValue() != null) {
                    nilaiAlternatifJarakSlamet = dataSnapshot.child("alternatif-jarak-1").child("Slamet").getValue(Double.class);
                    bobotPrioritasJarakSlamet = bobotPrioritasA * nilaiAlternatifJarakSlamet;

                    nilaiTotalSlamet = nilaiTotalSlamet + bobotPrioritasJarakSlamet;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Slamet").getValue() != null) {
                    nilaiAlternatif2JarakSlamet = dataSnapshot.child("alternatif-jarak-2").child("Slamet").getValue(Double.class);
                    bobotPrioritas2JarakSlamet = bobotPrioritasA * nilaiAlternatif2JarakSlamet;

                    nilaiTotal2Slamet = nilaiTotal2Slamet + bobotPrioritas2JarakSlamet;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Slamet").getValue() != null) {
                    nilaiAlternatif3JarakSlamet = dataSnapshot.child("alternatif-jarak-3").child("Slamet").getValue(Double.class);
                    bobotPrioritas3JarakSlamet = bobotPrioritasA * nilaiAlternatif3JarakSlamet;

                    nilaiTotal3Slamet = nilaiTotal3Slamet + bobotPrioritas3JarakSlamet;
                }

                // Sumbing
                if (dataSnapshot.child("alternatif-jarak-1").child("Sumbing").getValue() != null) {
                    nilaiAlternatifJarakSumbing = dataSnapshot.child("alternatif-jarak-1").child("Sumbing").getValue(Double.class);
                    bobotPrioritasJarakSumbing = bobotPrioritasA * nilaiAlternatifJarakSumbing;

                    nilaiTotalSumbing = nilaiTotalSumbing + bobotPrioritasJarakSumbing;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Sumbing").getValue() != null) {
                    nilaiAlternatif2JarakSumbing = dataSnapshot.child("alternatif-jarak-2").child("Sumbing").getValue(Double.class);
                    bobotPrioritas2JarakSumbing = bobotPrioritasA * nilaiAlternatif2JarakSumbing;

                    nilaiTotal2Sumbing = nilaiTotal2Sumbing + bobotPrioritas2JarakSumbing;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Sumbing").getValue() != null) {
                    nilaiAlternatif3JarakSumbing = dataSnapshot.child("alternatif-jarak-3").child("Sumbing").getValue(Double.class);
                    bobotPrioritas3JarakSumbing = bobotPrioritasA * nilaiAlternatif3JarakSumbing;

                    nilaiTotal3Sumbing = nilaiTotal3Sumbing + bobotPrioritas3JarakSumbing;
                }

                // Telomoyo
                if (dataSnapshot.child("alternatif-jarak-1").child("Telomoyo").getValue() != null) {
                    nilaiAlternatifJarakTelomoyo = dataSnapshot.child("alternatif-jarak-1").child("Telomoyo").getValue(Double.class);
                    bobotPrioritasJarakTelomoyo = bobotPrioritasA * nilaiAlternatifJarakTelomoyo;

                    nilaiTotalTelomoyo = nilaiTotalTelomoyo + bobotPrioritasJarakTelomoyo;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Telomoyo").getValue() != null) {
                    nilaiAlternatif2JarakTelomoyo = dataSnapshot.child("alternatif-jarak-2").child("Telomoyo").getValue(Double.class);
                    bobotPrioritas2JarakTelomoyo = bobotPrioritasA * nilaiAlternatif2JarakTelomoyo;

                    nilaiTotal2Telomoyo = nilaiTotal2Telomoyo + bobotPrioritas2JarakTelomoyo;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Telomoyo").getValue() != null) {
                    nilaiAlternatif3JarakTelomoyo = dataSnapshot.child("alternatif-jarak-3").child("Telomoyo").getValue(Double.class);
                    bobotPrioritas3JarakTelomoyo = bobotPrioritasA * nilaiAlternatif3JarakTelomoyo;

                    nilaiTotal3Telomoyo = nilaiTotal3Telomoyo + bobotPrioritas3JarakTelomoyo;
                }

                // Ungaran
                if (dataSnapshot.child("alternatif-jarak-1").child("Ungaran").getValue() != null) {
                    nilaiAlternatifJarakUngaran = dataSnapshot.child("alternatif-jarak-1").child("Ungaran").getValue(Double.class);
                    bobotPrioritasJarakUngaran = bobotPrioritasA * nilaiAlternatifJarakUngaran;

                    nilaiTotalUngaran = nilaiTotalUngaran + bobotPrioritasJarakUngaran;
                }

                if (dataSnapshot.child("alternatif-jarak-2").child("Ungaran").getValue() != null) {
                    nilaiAlternatif2JarakUngaran = dataSnapshot.child("alternatif-jarak-2").child("Ungaran").getValue(Double.class);
                    bobotPrioritas2JarakUngaran = bobotPrioritasA * nilaiAlternatif2JarakUngaran;

                    nilaiTotal2Ungaran = nilaiTotal2Ungaran + bobotPrioritas2JarakUngaran;
                }

                if (dataSnapshot.child("alternatif-jarak-3").child("Ungaran").getValue() != null) {
                    nilaiAlternatif3JarakUngaran = dataSnapshot.child("alternatif-jarak-3").child("Ungaran").getValue(Double.class);
                    bobotPrioritas3JarakUngaran = bobotPrioritasA * nilaiAlternatif3JarakUngaran;

                    nilaiTotal3Ungaran = nilaiTotal3Ungaran + bobotPrioritas3JarakUngaran;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ArrayList<Integer> dataList = new ArrayList<>();

        mDatabase.child("recommendation").orderByChild("value-main").limitToLast(2).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() > 0) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        if (snapshot.getChildrenCount() > 0) {
                            dataList.add(snapshot.child("id").getValue(Integer.class));
                        }
                    }
                    Log.d(TAG, dataList.toString());

                    GunungModel data1 = dbHandler.getDetailGunung(dataList.get(1));
                    tvResult1.setText(data1.getGunung());
                    Glide.with(RecommendationResultActivity.this).load(data1.getFoto()).into(imgResult1);

                    GunungModel data2 = dbHandler.getDetailGunung(dataList.get(0));
                    tvResult2.setText(data2.getGunung());
                    Glide.with(RecommendationResultActivity.this).load(data2.getFoto()).into(imgResult2);

                    imgResult1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RecommendationResultActivity.this, DeskripsiGunungActivity.class);
                            intent.putExtra("ID_GUNUNG", dataList.get(1));
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        }
                    });

                    imgResult2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(RecommendationResultActivity.this, DeskripsiGunungActivity.class);
                            intent.putExtra("ID_GUNUNG", dataList.get(0));
                            startActivity(intent);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    });
                } else {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}