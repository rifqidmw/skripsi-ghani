package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.session.Session;

public class SplashActivity extends AppCompatActivity {

    Handler handler;
    Session session;
    DatabaseHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        dbHandler = new DatabaseHandler(SplashActivity.this);
        session = new Session(SplashActivity.this);

        boolean firstTime = session.getFirstTime("isFirstTime");
        if (firstTime){
            session.save("isFirstTime", false);
            dbHandler.addGunung();
        }

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, QuestionActivity.class));
            }
        }, 2000);
    }
}