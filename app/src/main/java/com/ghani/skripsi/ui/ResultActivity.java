package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;
import com.santalu.diagonalimageview.DiagonalImageView;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {
    TextView tvResult1, tvResult2;
    DiagonalImageView imgResult1, imgResult2;
    DatabaseHandler dbHandler;
    ArrayList<GunungModel> gunungList;
    Boolean isFirstCheck = true;
    Boolean isPlus = false;
    int flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        tvResult1 = findViewById(R.id.tv_result_1);
        tvResult2 = findViewById(R.id.tv_result_2);
        imgResult1 = findViewById(R.id.img_1);
        imgResult2 = findViewById(R.id.img_2);

        dbHandler = new DatabaseHandler(ResultActivity.this);
        Intent getIntent = getIntent();
        int resultValue = getIntent.getIntExtra("RESULT_VALUE", 0);

        gunungList = new ArrayList<>();

        flag = flag +resultValue;
        gunungList.addAll(dbHandler.getGunung(flag, 2));

        Log.e("FLAGSIZE123", Integer.toString(flag));

        checkSizeResultGunung();
    }

    private void checkSizeResultGunung(){
        Log.e("FLAGSIZE", Integer.toString(flag));
        if (gunungList.size() < 2){
            if (isFirstCheck){
                isFirstCheck = false;
                if (flag > 3 && flag <= 12){
                    isPlus = false;
                    flag--;
                } else {
                    isPlus = true;
                    flag++;
                }
            } else {
                if (isPlus){
                    flag++;
                } else {
                    flag--;
                }
            }

            gunungList.addAll(dbHandler.getGunung(flag, 1));

            getResultGunung();
        } else {
            getResultGunung();
        }
    }

    private void getResultGunung(){

        Log.e("GUNUNGSIZE", Integer.toString(gunungList.size()));
        if (gunungList.size() < 2){
            checkSizeResultGunung();
        } else{
            StringBuffer sb1 = new StringBuffer();
            sb1.append("Gunung"+ "\n" + gunungList.get(0).getGunung() );
            StringBuffer sb2 = new StringBuffer();
            sb2.append("Gunung"+ "\n" + gunungList.get(1).getGunung() );

            //set text nama gunung
            tvResult1.setText(sb1);
            tvResult2.setText(sb2);
            //set image gunung
            Glide.with(ResultActivity.this).load(gunungList.get(0).getFoto()).into(imgResult1);
            Glide.with(ResultActivity.this).load(gunungList.get(1).getFoto()).into(imgResult2);

            imgResult1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ResultActivity.this, DeskripsiGunungActivity.class);
                    intent.putExtra("ID_GUNUNG", gunungList.get(0).getId());
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
            });

            imgResult2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ResultActivity.this, DeskripsiGunungActivity.class);
                    intent.putExtra("ID_GUNUNG", gunungList.get(1).getId());
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            });
        }

    }
}