package com.ghani.skripsi.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ghani.skripsi.R;
import com.ghani.skripsi.database.DatabaseHandler;
import com.ghani.skripsi.model.GunungModel;

public class EstimasiActivity extends AppCompatActivity {
    DatabaseHandler dbHandler;
    ImageView imgGunung;
    TextView tvEstimasi;
    Button btnMengerti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimasi);

        imgGunung = findViewById(R.id.img_gunung);
        tvEstimasi = findViewById(R.id.tv_estimasi);
        btnMengerti = findViewById(R.id.btn_mengerti);

        dbHandler = new DatabaseHandler(EstimasiActivity.this);
        Intent getIntent = getIntent();

        GunungModel gunungModel = dbHandler.getDetailGunung(getIntent.getIntExtra("ID_GUNUNG", 0));

        Glide.with(EstimasiActivity.this).load(gunungModel.getFoto()).into(imgGunung);

        if (!gunungModel.getEstimasi_biaya().equals("")){
            tvEstimasi.setText(gunungModel.getEstimasi_biaya());
        } else {
            tvEstimasi.setText("Peralatan tidak tersedia");
        }

        btnMengerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}